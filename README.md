# Project Name

`python manage.py shell_plus --notebook`

Run `pip install -r requirements.txt` in your virtual environment.

# How to Deploy in App Engine

`gcloud app deploy -v 1-0-13 --project coinfolio-live`

### Deploying Cron Job

`gcloud app deploy cron.yaml`
