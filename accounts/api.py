from rest_framework import status
from rest_framework.decorators import api_view

from accounts.models import UserProfile
from accounts.utils.json import user_json
from growth_authentication.models import GrowthUser
from growth_base_server.api.utils import require_params, fail_response, create_api_user, success_response, \
    login_api_user
from growth_base_server.exceptions import MissingParam
from portfolios.models import Wallet


@api_view(['POST', 'GET'])
def signup(request, **kwargs):
    try:
        required_params = ('email', 'password', 'wallet_address')
        email, password, wallet_address = require_params(request, required_params)
    except MissingParam as mp:
        return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)

    wallet_address = wallet_address.lower()

    if GrowthUser.objects.filter(email=email).exists():
        return fail_response('An account with that email already exists.', status_code=status.HTTP_403_FORBIDDEN)

    user = create_api_user(email, password)
    user.set_login_token()
    user.save()

    wallet, created = Wallet.objects.get_or_create(address=wallet_address)

    if created or not wallet.users.filter(id=request.user.id).exists():
        wallet.users.add(user)

    UserProfile.objects.create(user=user)

    data = user_json(user)
    return success_response('User Profile Created', data=data)


@api_view(['POST', 'GET'])
def login(request, **kwargs):
    try:
        required_params = ('email', 'password')
        email, password = require_params(request, required_params)
    except MissingParam as mp:
        return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)

    user = login_api_user(request, email, password)

    if user is None and GrowthUser.objects.filter(email=email, is_active=False).exists():
        return fail_response('Account Not Verified. Needs to be Activated', status_code=status.HTTP_401_UNAUTHORIZED)
    elif user is None:
        return fail_response("Invalid email or password", status_code=status.HTTP_401_UNAUTHORIZED)

    data = user_json(user)
    return success_response('Login Successful', data=data)
