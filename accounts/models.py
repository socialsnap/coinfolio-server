from django.db import models

from growth_authentication.models import GrowthUser


class UserProfile(models.Model):
    class Meta:
        db_table = 'coinfolio_userprofile'
        verbose_name_plural = 'userprofiles'

    user = models.OneToOneField(GrowthUser, on_delete=models.CASCADE)

    created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    updated = models.DateTimeField(null=False, blank=True, auto_now=True)

    def __str__(self):
        return self.user.get_username()
