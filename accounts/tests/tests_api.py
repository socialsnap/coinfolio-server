from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status

from accounts.models import UserProfile
from portfolios.models import Wallet


class TestSignUp(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-accounts:signup', kwargs={'version': 1})
        self.growth_user = mommy.make('GrowthUser', is_api_user=True, is_dashboard_user=False, _fill_optional=True)

        self.json = {
            'email': 'test@email.com',
            'password': 'test-password',
            'wallet_address': 'test-wallet-address'
        }

    def test_success(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertContains(response, 'User Profile Created', status_code=status.HTTP_200_OK)

    def test_contains_correct_data(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertContains(response, 'token', status_code=status.HTTP_200_OK)
        self.assertContains(response, 'email', status_code=status.HTTP_200_OK)
        self.assertContains(response, 'is_active', status_code=status.HTTP_200_OK)

    def test_sets_correct_login_token(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        user_token = response.json()['data']['token']
        self.assertIsNotNone(user_token)

    def test_contains_active_user(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEquals(response.json()['data']['is_active'], True)

    def test_profile_create_success(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        user_token = response.json()['data']['token']

        user_profile = UserProfile.objects.filter(user__login_token=user_token)
        self.assertTrue(user_profile.exists())

    def test_creates_new_wallet(self):
        self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEquals(Wallet.objects.count(), 1)
        self.assertEquals(Wallet.objects.first().address, self.json['wallet_address'])

    def test_links_user_when_wallet_created(self):
        self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEquals(Wallet.objects.first().users.count(), 1)

    def test_links_user_wallet_already_exists(self):
        mommy.make(Wallet, address=self.json['wallet_address'])
        self.assertEquals(Wallet.objects.first().users.count(), 0)

        self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEquals(Wallet.objects.first().users.count(), 1)

    def test_returns_403_when_user_already_exists(self):
        self.json['email'] = self.growth_user.email
        response = self.client.post(self.URL_ENDPOINT, self.json)

        self.assertContains(response, 'An account with that email already exists.',
                            status_code=status.HTTP_403_FORBIDDEN)

        self.json['email'] = self.growth_user.email
        response = self.client.post(self.URL_ENDPOINT, self.json)

        self.assertContains(response, 'An account with that email already exists.',
                            status_code=status.HTTP_403_FORBIDDEN)

    def test_missing_param(self):
        del self.json['email']
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        del self.json['password']
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestLogin(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-accounts:login', kwargs={'version': 1})
        self.growth_user = mommy.make('GrowthUser', email='test@email.com', username='test@email.com', is_active=True,
                                      is_api_user=True, is_dashboard_user=False, _fill_optional=True)

        self.growth_user.set_password('test-password')
        self.growth_user.save()

        self.wallet = Wallet.objects.create(address='test-wallet-address')
        self.wallet.users.add(self.growth_user)
        self.wallet.save()

        self.json = {
            'email': self.growth_user.email,
            'password': 'test-password',
        }

    def test_missing_param(self):
        del self.json['email']
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        del self.json['password']
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_success(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertContains(response, 'Login Successful', status_code=status.HTTP_200_OK)

    def test_contains_correct_data(self):
        response = self.client.post(self.URL_ENDPOINT, self.json)

        self.assertEquals(response.json()['data']['token'], self.growth_user.login_token)
        self.assertEquals(response.json()['data']['email'], self.growth_user.email)
        self.assertEquals(len(response.json()['data']['wallet_addresses']), 1)
        self.assertEquals(response.json()['data']['wallet_addresses'][0], self.wallet.address)
        self.assertEquals(response.json()['data']['is_active'], True)
