from django.urls import path, include

from accounts.api import signup, login

accounts_api_patterns = [
    path('/create', signup, name='signup'),
    path('/login', login, name='login'),
]
