from growth_base_server.api.utils import get_param


def process_profile_image_request(request, profile):
    image_url = get_param(request, 'profile_image_url')
    if image_url:
        profile.profile_image_url = image_url
        profile.save()
