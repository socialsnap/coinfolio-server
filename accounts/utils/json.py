def user_json(user):
    wallet_addresses = []

    if user.wallet_set.count():
        wallet_addresses = [wallet.address for wallet in user.wallet_set.all()]

    return {
        'token': user.login_token,
        'email': user.email,
        'is_active': user.is_active,
        'wallet_addresses': wallet_addresses,
    }
