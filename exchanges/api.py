# from binance.client import Client
# from rest_framework import status
# from rest_framework.decorators import api_view
#
# from exchanges.models import Exchange
# from exchanges.utils.binance_utils import user_assets_json, set_stop_loss_json
# from exchanges.utils.json import exchange_json
# from growth_base_server.api.utils import fail_response, success_response, authenticate_api_user, require_params
# from growth_base_server.exceptions import MissingParam
#
#
# @api_view(['POST', 'GET'])
# @authenticate_api_user()
# def edit(request, **kwargs):
#     try:
#         required_params = ('api_key', 'api_secret')
#         api_key, api_secret = require_params(request, required_params)
#     except MissingParam as mp:
#         return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)
#
#     exchange, _ = Exchange.objects.update_or_create(
#         user=request.user, platform=Exchange.PLATFORM_BINANCE,
#         defaults={
#             'user': request.user,
#             'platform': Exchange.PLATFORM_BINANCE,
#             'api_key': api_key,
#             'api_secret': api_secret,
#         },
#     )
#     data = exchange_json(exchange)
#     return success_response('Exchange Info updated.', data=data)
#
#
# @api_view(['POST', 'GET'])
# @authenticate_api_user()
# def platform_info(request, **kwargs):
#     try:
#         exchange = Exchange.objects.get(user=request.user, platform=Exchange.PLATFORM_BINANCE)
#     except Exchange.DoesNotExist:
#         return fail_response('Exchange does not exist.', status_code=status.HTTP_404_NOT_FOUND)
#
#     data = exchange_json(exchange)
#     return success_response('Exchange info.', data=data)
#
#
# @api_view(['POST', 'GET'])
# @authenticate_api_user()
# def order_stop_loss(request, **kwargs):
#     try:
#         required_params = ('symbol', 'stop_price', 'price', 'quantity')
#         symbol, stop_price, price, quantity = require_params(request, required_params)
#     except MissingParam as mp:
#         return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)
#
#     try:
#         exchange = Exchange.objects.get(user=request.user, platform=Exchange.PLATFORM_BINANCE)
#     except Exchange.DoesNotExist:
#         return fail_response('Exchange does not exist.', status_code=status.HTTP_404_NOT_FOUND)
#
#     client = Client(exchange.api_key, exchange.api_secret)
#
#     try:
#         data = set_stop_loss_json(client, symbol, quantity, stop_price, price)
#     except Exception as error:
#         return fail_response(f'Binance API Error: {str(error)}', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
#     return success_response('Stop loss order success.', data=data)
#
#
# @api_view(['POST', 'GET'])
# @authenticate_api_user()
# def user_assets(request, **kwargs):
#     try:
#         exchange = Exchange.objects.get(user=request.user, platform=Exchange.PLATFORM_BINANCE)
#     except Exchange.DoesNotExist:
#         return fail_response('Exchange does not exist.', status_code=status.HTTP_404_NOT_FOUND)
#
#     client = Client(exchange.api_key, exchange.api_secret)
#
#     try:
#         data = user_assets_json(client)
#     except Exception as error:
#         return fail_response(f'Binance Error: {str(error)}', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
#     return success_response('User Assets.', data=data)
