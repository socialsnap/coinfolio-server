# from django.db import models
#
# from growth_authentication.models import GrowthUser
#
#
# class Exchange(models.Model):
#     PLATFORM_BINANCE = 'binance'
#
#     PLATFORM_CHOICES = (
#         (PLATFORM_BINANCE, 'Binance'),
#     )
#
#     class Meta:
#         db_table = 'coinfolio_exchange'
#         verbose_name_plural = 'exchanges'
#
#     user = models.OneToOneField(GrowthUser, on_delete=models.CASCADE)
#     platform = models.CharField(max_length=50, choices=PLATFORM_CHOICES, null=False, blank=False)
#
#     api_key = models.CharField(max_length=255, null=True, blank=True)
#     api_secret = models.CharField(max_length=255, null=True, blank=True)
#
#     created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
#     updated = models.DateTimeField(null=False, blank=True, auto_now=True)
#
#     def __str__(self):
#         return f'{self.platform.title()}<{self.user.get_username()}>'
