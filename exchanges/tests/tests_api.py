# from django.test import TestCase
# from django.urls import reverse
# from model_mommy import mommy
# from rest_framework import status
# from unittest.mock import patch
#
#
# class TestUserAssets(TestCase):
#     def setUp(self):
#         self.URL_ENDPOINT = reverse('api-exchanges:api-binance:user-assets', kwargs={'version': 1})
#         self.user = mommy.make('GrowthUser', is_api_user=True, is_dashboard_user=False, _fill_optional=True)
#         self.exchange = mommy.make('Exchange', user=self.user, platform='binance', api_key='test-api-key',
#                                    api_secret='test-api-secret')
#
#         self.json = {
#             'token': self.user.login_token,
#         }
#
#     def test_binance_returns_500(self):
#         with patch('exchanges.api.user_assets_json') as mock_user_assets_json:
#             mock_user_assets_json.side_effect = Exception('Binance Error: Error fetching client account')
#
#             response = self.client.post(self.URL_ENDPOINT, self.json)
#             assert mock_user_assets_json.called
#
#             self.assertContains(response, 'Binance Error: Error fetching client account',
#                                 status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
#     def test_success(self):
#         with patch('exchanges.api.user_assets_json') as mock_user_assets_json:
#             mock_user_assets_json.return_value = {
#                 'portfolio_total': 154.03313451385094,
#                 'wallet':
#                     [
#                         {
#                             'coin': 'BNB',
#                             'amount': 0.578008,
#                             'value': 154.03313451385094,
#                             'price': 266.48962387,
#                             'stop_loss_price': 266.48962387
#                         }
#                     ]
#             }
#
#             response = self.client.post(self.URL_ENDPOINT, self.json)
#             assert mock_user_assets_json.called
#
#             self.assertContains(response, 'User Assets.', status_code=status.HTTP_200_OK)
#             self.assertIn('portfolio_total', response.json()['data'].keys())
#             self.assertIn('wallet', response.json()['data'].keys())
#
#     def test_unauthorized_access_returns_401(self):
#         self.json['token'] = 'wrong-token'
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         del self.json['token']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_returns_404_when_exchange_unavailable(self):
#         self.exchange.delete()
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertContains(response, 'Exchange does not exist.', status_code=status.HTTP_404_NOT_FOUND)
#
#
# class TestPlatformInfo(TestCase):
#     def setUp(self):
#         self.URL_ENDPOINT = reverse('api-exchanges:api-binance:platform-info', kwargs={'version': 1})
#         self.user = mommy.make('GrowthUser', is_api_user=True, is_dashboard_user=False, _fill_optional=True)
#         self.exchange = mommy.make('Exchange', user=self.user, platform='binance', api_key='test-api-key',
#                                    api_secret='test-api-secret')
#
#         self.json = {
#             'token': self.user.login_token,
#         }
#
#     def test_success(self):
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#
#         self.assertContains(response, 'Exchange info.', status_code=status.HTTP_200_OK)
#         self.assertIn('name', response.json()['data'].keys())
#         self.assertEqual('binance', response.json()['data']['name'])
#         self.assertIn('api_key', response.json()['data'].keys())
#         self.assertEqual('test-api-key', response.json()['data']['api_key'])
#         self.assertIn('api_secret', response.json()['data'].keys())
#         self.assertEqual('test-api-secret', response.json()['data']['api_secret'])
#
#     def test_no_exchange(self):
#         self.exchange.delete()
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertContains(response, 'Exchange does not exist.', status_code=status.HTTP_404_NOT_FOUND)
#
#     def test_unauthorized_access_returns_401(self):
#         self.json['token'] = 'wrong-token'
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         del self.json['token']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#
# class TestEdit(TestCase):
#     def setUp(self):
#         self.URL_ENDPOINT = reverse('api-exchanges:api-binance:edit', kwargs={'version': 1})
#         self.growth_user = mommy.make('GrowthUser', is_api_user=True, is_dashboard_user=False, _fill_optional=True)
#
#         self.json = {
#             'token': self.growth_user.login_token,
#             'api_key': 'test-api-key',
#             'api_secret': 'test-api-secret'
#         }
#
#     def test_success(self):
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#
#         self.assertContains(response, 'Exchange Info updated.', status_code=status.HTTP_200_OK)
#         self.assertIn('name', response.json()['data'].keys())
#         self.assertEqual('binance', response.json()['data']['name'])
#         self.assertIn('api_key', response.json()['data'].keys())
#         self.assertEqual('test-api-key', response.json()['data']['api_key'])
#         self.assertIn('api_secret', response.json()['data'].keys())
#         self.assertEqual('test-api-secret', response.json()['data']['api_secret'])
#
#     def test_unauthorized_access_returns_401(self):
#         self.json['token'] = 'wrong-token'
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         del self.json['token']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_missing_param(self):
#         del self.json['api_key']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#
#         self.json['api_key'] = 'test-api-key'
#         del self.json['api_secret']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#
#     def test_updates_binance_credentials(self):
#         self.exchange = mommy.make('Exchange', user=self.growth_user, platform='binance', api_key='test-api-key-old',
#                                    api_secret='test-api-secret-old')
#
#         self.assertNotEqual(self.exchange.api_key, self.json['api_key'])
#         self.assertNotEqual(self.exchange.api_secret, self.json['api_secret'])
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#
#         self.assertContains(response, 'Exchange Info updated.', status_code=status.HTTP_200_OK)
#         self.assertEqual('test-api-key', response.json()['data']['api_key'])
#         self.assertEqual('test-api-secret', response.json()['data']['api_secret'])
#
#
# class TestStopLoss(TestCase):
#     def setUp(self):
#         self.URL_ENDPOINT = reverse('api-exchanges:api-binance:api-order:set-stop-loss', kwargs={'version': 1})
#         self.user = mommy.make('GrowthUser', is_api_user=True, is_dashboard_user=False, _fill_optional=True)
#         self.exchange = mommy.make('Exchange', user=self.user, platform='binance', api_key='test-api-key',
#                                    api_secret='test-api-secret')
#
#         self.json = {
#             'token': self.user.login_token,
#             'symbol': 'TESTUSDT',
#             'stop_price': 0.0001,
#             'price': 0.0002,
#             'quantity': 1,
#         }
#
#     def test_success(self):
#         with patch('exchanges.api.set_stop_loss_json') as mock_set_stop_loss_json:
#             mock_set_stop_loss_json.return_value = {
#                 "symbol": "TESTUSDT",
#                 "orderId": 28,
#                 "clientOrderId": "6gCrw2kRUAF9CvJDGP16IP",
#                 "transactTime": 1507725176595,
#                 "price": "0.0001",
#                 "origQty": "0.0002",
#                 "executedQty": "1",
#                 "status": "FILLED",
#                 "timeInForce": "GTC",
#                 "type": "STOP_LOSS_LIMIT",
#                 "side": "SELL"
#             }
#
#             response = self.client.post(self.URL_ENDPOINT, self.json)
#             assert mock_set_stop_loss_json.called
#             self.assertContains(response, 'Stop loss order success.', status_code=status.HTTP_200_OK)
#
#     def test_binance_returns_500(self):
#         with patch('exchanges.api.set_stop_loss_json') as mock_set_stop_loss_json:
#             mock_set_stop_loss_json.side_effect = Exception('BinanceRequestException: Error msg')
#
#             response = self.client.post(self.URL_ENDPOINT, self.json)
#             assert mock_set_stop_loss_json.called
#
#             self.assertContains(response, 'Binance API Error: BinanceRequestException: Error msg',
#                                 status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
#     def test_unauthorized_access_returns_401(self):
#         self.json['token'] = 'wrong-token'
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         del self.json['token']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_missing_param(self):
#         del self.json['symbol']
#         response = self.client.post(self.URL_ENDPOINT, self.json)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
