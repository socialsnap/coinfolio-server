# import math
#
# from binance.enums import TIME_IN_FORCE_GTC, SIDE_SELL, ORDER_TYPE_STOP_LOSS_LIMIT
# from binance.exceptions import BinanceAPIException, BinanceRequestException, BinanceOrderException
#
#
# def _ticker_price_filter(tickers, symbol):
#     price = None
#     for item in tickers:
#         if item['symbol'] == symbol:
#             price = float(item['price'])
#
#     return price
#
#
# def user_assets_json(client):
#     try:
#         account = client.get_account()
#     except Exception as error:
#         raise Exception(f'Error fetching client account. {str(error)}')
#
#     assets = []
#     portfolio_total = 0
#
#     try:
#         tickers = client.get_all_tickers()
#     except Exception as error:
#         raise Exception(f'Error getting all tickers: {str(error)}')
#
#     for balance in account['balances']:
#         coin_name = balance['asset']
#         coins_free = float(balance['free'])
#         coins_locked = float(balance['locked'])
#         coins_owned = coins_free + coins_locked
#
#         if coin_name == 'USDT' or coins_owned <= 0:
#             continue
#
#         coin_price = _ticker_price_filter(tickers=tickers, symbol=f'{coin_name}USDT')
#         if coin_price is None:
#             continue
#
#         available_usd = coins_free * coin_price
#         total_usd = coins_owned * coin_price
#         portfolio_total += total_usd
#
#         assets.append({
#             'coin': balance['asset'],
#             'coins_total': coins_owned,
#             'coins_free': coins_free,
#             'total_usd': total_usd,
#             'available_usd': available_usd,
#             'price': coin_price,
#             'stop_loss_price': coin_price,
#         })
#
#     return {
#         'portfolio_total': portfolio_total,
#         'wallet': assets,
#     }
#
#
# def _quantity(client, symbol, quantity):
#     sym_info = client.get_symbol_info(symbol)
#     filters = sym_info['filters']
#
#     step_size = None
#     for f in filters:
#         if f['filterType'] == 'LOT_SIZE':
#             step_size = float(f['stepSize'])
#             break
#
#     precision = int(round(-math.log(step_size, 10), 0))
#     quantity = float(round(quantity * 0.9995, precision))
#     return quantity
#
#
# def set_stop_loss_json(client, symbol, quantity, stop_price, price):
#     symbol = f'{symbol}USDT'
#     quantity = _quantity(client, symbol, quantity)
#
#     try:
#         data = client.create_order(symbol=symbol, side=SIDE_SELL, type=ORDER_TYPE_STOP_LOSS_LIMIT,
#                                    timeInForce=TIME_IN_FORCE_GTC, quantity=quantity, stopPrice=stop_price, price=price)
#     except BinanceRequestException as error:
#         raise Exception(f'BinanceRequestException: {str(error)}')
#     except BinanceAPIException as error:
#         raise Exception(f'BinanceAPIException: {str(error)}')
#     except BinanceOrderException as error:
#         raise Exception(f'BinanceOrderException: {str(error)}')
#
#     return data
