from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import GrowthUser


class GrowthUserAdmin(UserAdmin):
    model = GrowthUser
    ADDITIONAL_FIELDS = ('is_api_user', 'is_dashboard_user', 'login_token')

    list_display = UserAdmin.list_display + ADDITIONAL_FIELDS
    fieldsets = UserAdmin.fieldsets + (
            ('Additional Fields', {'fields': ADDITIONAL_FIELDS}),
    )


admin.site.register(GrowthUser, GrowthUserAdmin)
