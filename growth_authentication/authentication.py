from django.contrib.auth.backends import ModelBackend


class GrowthAuthentication(ModelBackend):
    user = None

    def authenticate(self, request, username=None, password=None, **kwargs):
        user = super(GrowthAuthentication, self).authenticate(request, username, password, **kwargs)

        if user and (user.is_dashboard_user or user.is_staff):
            return user


class ApplicationUserAuthentication(GrowthAuthentication):
    def authenticate(self, request, username=None, password=None, **kwargs):
        user = super(ApplicationUserAuthentication, self).authenticate(request, username, password, **kwargs)

        if user and not user.is_deleted and user.application_user.valid_company():
            return user
