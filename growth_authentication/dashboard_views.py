from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse, NoReverseMatch
from django.shortcuts import redirect
from django.views.generic import TemplateView

from growth_authentication.models import GrowthUser
from growth_authentication.settings import GROWTH_LOGIN_TEMPLATE, GROWTH_DASHBOARD_TEMPLATE, \
    GROWTH_RESET_PASSWORD_TEMPLATE
from growth_base_server.dashboard import dashboard_config
from growth_base_server.emails import send_reset_password_email


class DashboardLogin(TemplateView):
    template_name = GROWTH_LOGIN_TEMPLATE
    base_template = GROWTH_DASHBOARD_TEMPLATE
    error_message = ''

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username').lower()
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(request, user)

            next_page = self.get_user_dashboard_home(user)

            return redirect(next_page)
        else:
            self.error_message = 'Invalid login.'
            return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(DashboardLogin, self).get_context_data()
        context['error_message'] = self.error_message
        context['base_template'] = self.base_template
        context['page'] = 'Login'
        context.update(dashboard_config.get_base_context(self.request))

        return context

    def get_user_dashboard_home(self, user):
        home = determine_redirect_page(user)

        if home is None:
            home = reverse('dashboard-home')

        return home


class DashboardResetPassword(TemplateView):
    template_name = GROWTH_RESET_PASSWORD_TEMPLATE
    base_template = GROWTH_DASHBOARD_TEMPLATE
    error_message = ''
    confirmation_message = ''

    def post(self, request, *args, **kwargs):
        try:
            email_address = request.POST.get('email').lower()
            user = GrowthUser.objects.get(email=email_address)
            token = user.generate_reset_password_token()
            sent = send_reset_password_email(user, token)
            if sent:
                self.confirmation_message = 'A link to reset your password has been sent to %s. Please check your email.' % (
                email_address,)
            else:
                self.error_message = 'Could not send reset link. Please try again later.'
        except ObjectDoesNotExist:
            self.error_message = 'Invalid user account. Could not reset password.'

        return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(DashboardResetPassword, self).get_context_data()
        context['error_message'] = self.error_message
        context['confirmation_message'] = self.confirmation_message
        context['base_template'] = self.base_template
        context['page'] = 'Login'
        context.update(dashboard_config.get_base_context(self.request))

        return context

    def get_user_dashboard_home(self, user):
        return reverse('dashboard-home')


class DashboardLogout(TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/dashboard/login/')


def determine_redirect_page(user):
    next_page = None

    menus = dashboard_config.user_menus

    try:
        for group in user.groups.all():
            if menus.has_key(group.name):
                next_page = reverse(menus[group.name][0])
    except NoReverseMatch:
        next_page = None

    return next_page
