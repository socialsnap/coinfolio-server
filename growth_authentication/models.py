import six
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import User, AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.crypto import salted_hmac

from growth_base_server.api.models import DEVICE_TYPE_CHOICES, DEVICE_IOS, DEVICE_ANDROID
from growth_notifications.models import APNSDevice, GCMDevice


class GrowthUser(AbstractUser):
    is_api_user = models.BooleanField(default=True)
    is_dashboard_user = models.BooleanField(default=False)

    login_token = models.CharField(max_length=128, null=True, blank=True)

    # used primarily by dashboard users:
    local_time_offset = models.IntegerField(default=0)

    device_token = models.CharField(max_length=200, null=True, blank=True)
    device_type = models.IntegerField(choices=DEVICE_TYPE_CHOICES, null=True, blank=True)

    def __str__(self):
        # return self.get_full_name()
        return self.email

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.email = self.email.lower()
        self.enforce_unique_email_address()

        return super(GrowthUser, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    def enforce_unique_email_address(self):
        email_is_used = False

        other_accounts = self.__class__.objects.filter(email=self.email, is_active=True)

        if self.pk:
            if other_accounts.exclude(pk=self.pk).exists():
                email_is_used = True
        else:
            if other_accounts.exists():
                email_is_used = True

        if email_is_used:
            raise ValidationError('An account already exists for this email address.')

    def authenticate_password(self, password):
        if not check_password(password, self.password):
            return False

        # LOGGED IN:
        # check and set login token accordingly
        if not self.authenticate_token(self.login_token):
            self.set_login_token()
            self.save()
        return True

    def authenticate_token(self, authentication_token):
        try:
            (id, test_token) = authentication_token.split('--')
            if self.pk != int(id):
                return False
            return self.generate_login_token() == authentication_token
        except:
            return False

    def generate_login_token(self):
        # Using values from the user record ensures that the token
        # belongs to the right user and that login tokens become
        # invalid if the password is changed
        key_salt = "growth_authentication.models.UserTokenGenerator"

        value = (six.text_type(self.pk) + self.password +
                 six.text_type(self.is_active) + six.text_type(self.email))
        hash = salted_hmac(key_salt, value).hexdigest()

        return str(self.pk) + '--' + str(hash)

    def set_login_token(self):
        self.login_token = self.generate_login_token()

    def generate_reset_password_token(self):
        # This token will become invalid if the password is changed or if the user successfully logs in
        key_salt = "growth_authentication.models.UserTokenGenerator"

        value = (six.text_type(self.pk) + self.password +
                 six.text_type(self.last_login) +
                 six.text_type(self.is_active) +
                 six.text_type(self.email))

        hash = salted_hmac(key_salt, value).hexdigest()[::2]

        return hash

    def check_reset_password_token(self, token):
        return self.generate_reset_password_token() == token

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def send_notification(self, message, message_type):
        import logging
        logging.debug("Sending notification")
        # noinspection PyBroadException
        try:
            device = None
            if self.device_type == DEVICE_IOS:
                device, created = APNSDevice.objects.get_or_create(registration_id=self.device_token)
            elif self.device_type == DEVICE_ANDROID:
                device, created = GCMDevice.objects.get_or_create(registration_id=self.device_token)

            if device and self.device_type == DEVICE_IOS:
                message_action = {'type': message_type}
                device.send_message(message, sound='default', extra=message_action)
                logging.debug("Sent iOS notification")
            elif device and self.device_type == DEVICE_ANDROID:
                message_action = {'click_action': message_type, 'android_channel_id': 'message', 'sender_id': self.id}
                device.send_message(message, extra=message_action)
                logging.debug("Sent Android notification")
        except Exception as e:
            logging.error("Error in sending notification:")
            logging.error(e)
            logging.error(self.device_type)
            pass
