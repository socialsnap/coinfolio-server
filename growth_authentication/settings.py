from django.conf import settings

GROWTH_AUTHENTICATION_CLASSES = getattr(settings, "GROWTH_AUTHENTICATION_CLASSES", {})

GROWTH_DASHBOARD_TEMPLATE = getattr(settings, "GROWTH_DASHBOARD_TEMPLATE", 'dashboard_base.html')
GROWTH_LOGIN_TEMPLATE = getattr(settings, "GROWTH_LOGIN_TEMPLATE", 'dashboard_login.html')
GROWTH_RESET_PASSWORD_TEMPLATE = getattr(settings, "GROWTH_RESET_PASSWORD_TEMPLATE", 'dashboard_reset_password.html')

GROWTH_ADMIN_EMAIL = getattr(settings, "GROWTH_ADMIN_EMAIL", 'projects@growthcreate.com')
