from django.contrib import admin
from django.contrib import messages
from django.urls import reverse
from django.utils.safestring import mark_safe

from growth_base_server.admin_forms import ApplicationCompanyAdminForm
from growth_base_server.emails import send_new_admin_email, send_set_password_email
from growth_base_server.models import ApplicationUser, ApplicationCompany, ImagePermissions
from growth_base_server.api.models import UnsupportedAppVersion


class ApplicationCompanyAdmin(admin.ModelAdmin):
    form = ApplicationCompanyAdminForm

    def add_view(self, request, form_url='', extra_context=None):
        "The 'add' admin view for this model."
        from google.appengine.ext import blobstore

        opts = self.model._meta
        try:
            upload_url = blobstore.create_upload_url(reverse('admin:%s_%s_add' %
                                                             (opts.app_label, opts.model_name),
                                                             current_app=self.admin_site.name))
        except Exception:
            upload_url = ''

        # set form_url to the blobstore upload url
        return super(ApplicationCompanyAdmin, self).add_view(request, form_url=upload_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        "The 'change' admin view for this model."
        from google.appengine.ext import blobstore

        opts = self.model._meta
        try:
            upload_url = blobstore.create_upload_url(
                reverse('admin:%s_%s_change' %
                        (opts.app_label, opts.model_name), args=(object_id,),
                        current_app=self.admin_site.name))
        except Exception:
            upload_url = ''

        # set form_url to the blobstore upload url
        return super(ApplicationCompanyAdmin, self).change_view(request, object_id, form_url=upload_url,
                                                                extra_context=None)


class ApplicationUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'application_company', 'is_deleted', 'send_email_button')

    def get_queryset(self, request):
        # we want to have deleted objects show up
        return self.model.objects.all_with_deleted()

    def send_email_button(self, obj):
        return mark_safe('<button type="submit" name="email_password"'
                         ' value="' + str(obj.pk) + '" class="default">Send Set Password Email</button')

    send_email_button.short_description = 'Email Password Link'
    send_email_button.allow_tags = True

    def changelist_view(self, request, extra_context=None):
        try:
            if request.method == 'POST' and '_save' not in request.POST and 'email_password' in request.POST:
                # 1. Figure out which button was pushed
                user_id = request.POST.get('email_password')
                user = ApplicationUser.objects.get(pk=user_id)

                # 2. Send email
                token = user.generate_reset_password_token()
                if user.is_deleted:
                    msg = "Cannot send email to a deleted user."
                    self.message_user(request, msg, messages.WARNING)
                if user.is_dashboard_user:
                    send_new_admin_email(user, token)
                    msg = "Email has been sent."
                    self.message_user(request, msg, messages.SUCCESS)
                else:
                    send_set_password_email(user, token)
                    msg = "Email has been sent."
                    self.message_user(request, msg, messages.SUCCESS)

        except:
            msg = "Error sending email."
            self.message_user(request, msg, messages.WARNING)

        return super(ApplicationUserAdmin, self).changelist_view(request, extra_context)


class UnsupportedAppVersionAdmin(admin.ModelAdmin):
    fields = ('app_version', 'device_type', 'cut_off_date')


class EndpointAdmin(admin.ModelAdmin):
    pass


admin.site.register(ApplicationUser, ApplicationUserAdmin)
admin.site.register(ApplicationCompany, ApplicationCompanyAdmin)
admin.site.register(ImagePermissions)
