from django import forms
from django.forms import widgets
from django.utils.safestring import mark_safe

from growth_base_server.models import ApplicationCompany
from growth_base_server.settings import settings


class BlobImageWidget(widgets.TextInput):
    def render(self, name, value, attrs=None):
        import socket

        try:
            HOSTNAME = socket.gethostname()
        except:
            HOSTNAME = 'localhost'
        if not value:
            thumb_uri = 'http://%s/static/dashboard/images/camera_icon.png' % HOSTNAME
            output_value = ''
            width = '256'
        else:
            thumb_uri = value
            output_value = value
            width = 'auto'
        return mark_safe(
            u'''<span class="thumbnail"><img src="%s" id="%s_thumb" width="%s" /></span><input type="hidden" name="%s" id="id_%s" value="%s" />''' % (thumb_uri, name, width, name, name, output_value)
        )


class ThumbnailWidget(widgets.TextInput):
    def render(self, name, value, attrs=None):
        import socket

        try:
            HOSTNAME = socket.gethostname()
        except:
            HOSTNAME = 'localhost'
        if not value:
            thumb_uri = 'http://%s/static/dashboard/images/camera_icon.png' % HOSTNAME
            output_value = ''
        else:
            thumb_uri = settings.THUMBNAIL_URI_PATTERN % (HOSTNAME, str(value))
            output_value = value
        return mark_safe(
            u'''<span class="thumbnail"><img src="%s" id="%s_thumb" /></span><input type="hidden" name="%s" id="id_%s" value="%s" />''' % (thumb_uri, name, name, name, output_value)
        )


class ApplicationCompanyAdminForm(forms.ModelForm):
    logo_upload = forms.FileField(required=False)

    class Meta:
        model = ApplicationCompany
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ApplicationCompanyAdminForm, self).__init__(*args, **kwargs)
        self.fields['logo_blobstore_key'].widget = ThumbnailWidget()
        self.fields['logo_blobstore_key'].label = 'Logo Image'

    def save(self, commit=True):
        obj = super(ApplicationCompanyAdminForm, self).save(commit=False)

        image = self.cleaned_data['logo_upload']
        if image:
            # TODO: old logos are left orphaned by this logic. could find and delete.
            image_key = str( image.blobstore_info.key() )
            obj.logo_blobstore_key = image_key
        obj.save()
        self.save_m2m()
        return obj
