from rest_framework.decorators import api_view

from growth_base_server.api.utils import fail_response, success_response, get_param


@api_view(['GET', 'POST'])
def store_local_time_offset(request):
    local_time_offset = get_param(request, 'offset')
    if not hasattr(request.user, 'growth_user'):
        return fail_response('Cannot store offset for super user')

    request.user.local_time_offset = local_time_offset
    request.user.save()

    return success_response('Time offset saved.')
