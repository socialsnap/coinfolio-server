from django.contrib import admin

from growth_base_server.admin import UnsupportedAppVersionAdmin
from growth_base_server.api.models import UnsupportedAppVersion

admin.site.register(UnsupportedAppVersion, UnsupportedAppVersionAdmin)
