from datetime import date
from functools import wraps

from django.core.exceptions import ObjectDoesNotExist

from growth_authentication.models import GrowthUser
from growth_base_server.api.models import UnsupportedAppVersion, DEVICE_TYPE_CHOICES, DEVICE_ANDROID, DEVICE_IOS
from growth_base_server.api.utils import fail_response, available_attrs


def authenticate_api_token():
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            user_token = request.data.get('user_token', request.GET.get('user_token', None))
            if not user_token or '--' not in user_token:
                return fail_response('Invalid login.')

            (user_id, token) = user_token.split('--')
            try:
                user = GrowthUser.objects.get(pk=user_id)
            except ObjectDoesNotExist:
                return fail_response('Invalid login.')

            if not user.authenticate_token(user_token):
                return fail_response('Invalid login.')

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator


def check_app_version():
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            # set up values
            app_version = request.data.get('app_version', request.GET.get('app_version', None))
            device_type = request.data.get('device_type', request.GET.get('device_type', None))
            debug_data = {
                'app_version_supported': True,
                'debug_message': '',
                'submitted_data': {'app_version': app_version, 'device_type': device_type},
            }
            # if device failed to submit values, Fail request
            if app_version is None or device_type is None:
                debug_data['app_version_supported'] = False
                debug_data['debug_message'] = 'Missing app_version or device_type'
                return fail_response("Your application version is not supported.", debug_data)

            # if device_type is not int, Fail request
            try:
                device_type = int(device_type)
            except:
                debug_data['app_version_supported'] = False
                debug_data['debug_message'] = 'Invalid value for device_type. Could not cast as int.'
                return fail_response("Your application version is not supported.", debug_data)

            # if device_type is not Android or iOS, Fail request
            if device_type not in (DEVICE_IOS, DEVICE_ANDROID):
                debug_data['app_version_supported'] = False
                debug_data['debug_message'] = 'Invalid value for device_type'
                return fail_response("Your application version is not supported.", debug_data)

            # if app_version is blacklisted, Fail request
            is_blacklisted = UnsupportedAppVersion.objects.filter(app_version=app_version,
                                                                  device_type=int(device_type),
                                                                  cut_off_date__lte=date.today())
            if is_blacklisted:
                debug_data['app_version_supported'] = False
                debug_data['debug_message'] = 'Unsupported for App Version ' + str(app_version) + \
                                              ' on ' + str(DEVICE_TYPE_CHOICES[device_type][1])
                debug_data['system_data'] = {'cut_off_date': is_blacklisted[0].cut_off_date}
                return fail_response("Your application version is not supported.", debug_data)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator
