from django.db import models


DEVICE_UNSPECIFIED = 0
DEVICE_IOS = 1
DEVICE_ANDROID = 2

DEVICE_TYPE_CHOICES = (
    (DEVICE_UNSPECIFIED, 'Unspecified'),
    (DEVICE_IOS,   'iOS'),
    (DEVICE_ANDROID, 'Android')
)


class UnsupportedAppVersion(models.Model):
    app_version = models.CharField(max_length=100)
    device_type = models.IntegerField(choices=DEVICE_TYPE_CHOICES)
    cut_off_date = models.DateField()

    def __str__(self):
        (key, label) = DEVICE_TYPE_CHOICES[self.device_type]
        return self.app_version + ' ' + label
