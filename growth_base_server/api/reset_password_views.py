from django.contrib.auth import login, authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from growth_authentication.dashboard_views import determine_redirect_page
from growth_authentication.models import GrowthUser
from growth_base_server.api.utils import get_param, fail_response
from growth_base_server.emails import send_reset_password_email


@api_view(['GET', 'POST'])
def send_reset_password(request):
    try:
        email_address = get_param(request, 'email')
        user = GrowthUser.objects.get(email=email_address)
        token = user.generate_reset_password_token()
        sent = send_reset_password_email(user, token)
        if sent:
            return_json = {
                'success': 1,
                'message': 'Please check your email for a reset link.'
            }
            return Response(return_json)
        else:
            return fail_response('Could not send link: /reset-pw/?token='+str(user.pk) + '--' + str(token), status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except ObjectDoesNotExist:
        return fail_response('Invalid user account. Could not reset password.', status_code=status.HTTP_404_NOT_FOUND)


class ResetPasswordView(TemplateView):
    template_name = 'user/reset_password.html'
    message = ''

    def get_context_data(self, **kwargs):
        context = super(ResetPasswordView, self).get_context_data(**kwargs)

        try:
            token = self.request.GET.get('token')
            (user_id, reset_token) = token.split('--')
            user = GrowthUser.objects.get(id=user_id)
            if user.check_reset_password_token(reset_token):
                context['show_form'] = True
                context['user_id'] = user_id
                context['reset_token'] = reset_token
                context['message'] = self.message
            else:
                self.message = '<h1>Invalid token. Could not reset password.</h1><p>Please re-request a reset link or contact your administrator.</p>'
                context['show_form'] = False
                context['message'] = self.message
            return context
        except:
            context['message'] = 'There was a problem with your request. Please try again later.'
            return context

    def post(self, request, *args, **kwargs):
        if 'submit' in request.POST:  # user submitted form
            user_id = request.POST['user_id']
            reset_token = request.POST['reset_token']
            password = request.POST['password']

            if password != request.POST['confirm_password']:
                self.message = '<h1>Password fields did not match.</h1>'
                return render(request, self.template_name, self.get_context_data())
            if password == '':
                self.message = '<h1>You must enter a new password.</h1>'
                return render(request, self.template_name, self.get_context_data())

            user = GrowthUser.objects.get(id=user_id)

            if user.check_reset_password_token(reset_token):
                user.password = password
                user.save()

                user.auth_user.is_active = True
                user.auth_user.save()

                next_page = determine_redirect_page(user.auth_user)
                if next_page is not None:
                    user = authenticate(username=user.name, password=password)
                    login(request, user)
                    return redirect(next_page)
                else:
                    return HttpResponseRedirect('/reset-pw/success/')
            else:
                self.message = '<h1>Invalid token. Could not reset password.</h1>'

        return super(ResetPasswordView, self).get(request, *args, **kwargs)


# Confirm password was reset and direct user to login to their app
class ResetPasswordSuccessView(TemplateView):
    template_name = 'user/reset_password.html'
    message = '<h1>Your password was successfully changed</h1><p>You can now log into the application with your new password.</p>'

    def get_context_data(self, **kwargs):
        context = super(ResetPasswordSuccessView, self).get_context_data(**kwargs)
        context['show_form'] = False
        context['message'] = self.message
        return context
