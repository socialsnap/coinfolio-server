from django.conf.urls import url

from growth_base_server.api.views import api_login, ActivateAccountView, add_file, serve_file, serve_image, \
    add_image

api_patterns = [
    url(r'^account/login/$', api_login),
    url(r'^account/activate/$', ActivateAccountView.as_view()),
    url(r'^image/add/', add_image),
    url(r'^image/$', serve_image),
    url(r'^file/add/', add_file),
    url(r'^file/$', serve_file),
]