import copy
from functools import wraps, WRAPPER_ASSIGNMENTS
from importlib import import_module

import requests
import six
from django.conf import settings
from django.contrib.admin.sites import site
from django.contrib.auth import authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.utils.module_loading import module_has_submodule
from requests_toolbelt import MultipartEncoder
from rest_framework import status
from rest_framework.response import Response

from growth_authentication.models import GrowthUser
from growth_base_server.emails import send_activate_account_email
from growth_base_server.exceptions import MissingParam


# TODO: Fix the fix around support for this in django 3
def available_attrs(fn):
    """
    Return the list of functools-wrappable attributes on a callable.
    This is required as a workaround for http://bugs.python.org/issue3445
    under Python 2.
    """
    if six.PY3:
        return WRAPPER_ASSIGNMENTS
    else:
        return tuple(a for a in WRAPPER_ASSIGNMENTS if hasattr(fn, a))


def authenticate_api_user():
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            token = get_param(request, 'token')
            if not token:
                return fail_response('Invalid user', status_code=status.HTTP_401_UNAUTHORIZED)

            if not GrowthUser.objects.filter(login_token=token, is_api_user=True).exists():
                return fail_response('Invalid user', status_code=status.HTTP_401_UNAUTHORIZED)

            request.user = GrowthUser.objects.get(login_token=token, is_api_user=True)

            return view_func(request, *args, **kwargs)

        return _wrapped_view

    return decorator


def create_api_user(email, password, is_active=True):
    user = GrowthUser(
        email=email,
        username=email,
        is_active=is_active,
        is_api_user=True,
        is_dashboard_user=False,
    )

    user.set_password(password)
    user.save()

    return user


def login_api_user(request, email, password):
    try:
        user = authenticate(request, username=email, password=password)
    except ObjectDoesNotExist:
        return None

    if user and user.is_api_user:
        return user
    else:
        return None


def send_account_activation(user):
    token = user.generate_reset_password_token()
    sent = send_activate_account_email(user, token)

    return sent


def fail_response(message, data=None, status_code=status.HTTP_400_BAD_REQUEST):
    response_json = {
        'message': message,
        'data': data
    }
    return Response(response_json, status=status_code)


def success_response(message='', data=None, status_code=status.HTTP_200_OK):
    response_json = {
        'message': message,
        'data': data
    }
    return Response(response_json, status=status_code)


def permission_denied(message='', data=None, status_code=status.HTTP_403_FORBIDDEN):
    response_json = {
        'message': message,
        'data': data
    }
    return Response(response_json, status=status_code)


# checks for param in POST then in GET, it also checks for FILE
def get_param(request, param_name, default=None):
    return request.data.get(param_name, request.query_params.get(param_name, default))


# image upload functions
def extend_request_timeout(new_deadline_in_seconds=60):
    # override request timeout when using the requests package to send a lot of data
    from google.appengine.api import urlfetch
    urlfetch.set_default_fetch_deadline(new_deadline_in_seconds)


def reset_request_timeout():
    from google.appengine.api import urlfetch
    urlfetch.set_default_fetch_deadline(5)


def get_test_image_for_upload():
    image = './static/testing/cutecat.jpg'
    image_data = open(image, 'rb')
    return image_data


def format_jpg_for_upload(image, test=None):
    # format image data for upload
    if image is None and test is not None:
        return get_test_image_for_upload()
    elif image is None:
        return None

    upfile = SimpleUploadedFile(image.name, image.read(), "image/jpeg")
    image_data = upfile.read()
    return image_data


def require_params(request, param_list):
    returned_params = []
    for param in param_list:
        if get_param(request, param) is None or get_param(request, param) == '':
            raise MissingParam('Missing parameter', param, param_list)
        returned_params.append(get_param(request, param))
    return returned_params


def require_param(request, param):
    value = get_param(request, param)
    if value in (None, ""):
        raise MissingParam('Missing parameter', param, [param])
    return value


def get_params(request, param_list):
    returned_params = []
    for param in param_list:
        returned_params.append(get_param(request, param))
    return returned_params


def get_boolean_params(request, param_list):
    returned_params = []
    for param in param_list:
        param = get_param(request, param)
        if param in ('0', False, 'false', 'FALSE', 'False'):
            param = False
        elif param in ('1', True, 'true', 'TRUE', 'True'):
            param = True
        else:
            param = None
        returned_params.append(param)
    return returned_params


def save_to_blobstore(file, application_company, csrfmiddlewaretoken, content_type="text/csv"):
    import logging
    upfile = SimpleUploadedFile(file.name, file.read(), content_type)
    file_data = upfile.read()

    extend_request_timeout()
    from google.appengine.ext import blobstore
    upload_url = blobstore.create_upload_url(reverse('save-image'))
    logging.debug(reverse('save_blobstore_file'))
    m = MultipartEncoder(
        fields={
            'application_company_id': str(application_company.id),
            'file': (file.name, file_data, content_type),
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    )
    cookies = dict(csrftoken=csrfmiddlewaretoken)
    headers = {
        'X-CSRFToken': csrfmiddlewaretoken,
        'Content-Type': m.content_type
    }
    r = requests.post(upload_url, data=m, headers=headers, cookies=cookies)
    response_json = r.json()
    logging.debug(response_json)

    reset_request_timeout()

    return response_json['data']['blobstore_key']


def find_user_serializer(user):
    user_serializer = None

    # check INSTALLED_APPS for user serializer class
    for app in settings.INSTALLED_APPS:
        mod = import_module(app)
        # Attempt to import the app's serializers module.
        try:
            before_import_registry = copy.copy(site._registry)
            package = import_module('%s.serializers' % app)
            if hasattr(package, 'USER_SERIALIZER_CLASS'):
                user_serializer_class = getattr(package, 'USER_SERIALIZER_CLASS')

                user_serializer = user_serializer_class(user)
        except:
            # Reset the model registry to the state before the last import
            site._registry = before_import_registry

            # If the app doesn't have a serializers module, we can ignore
            # the error, otherwise we want it to bubble up.
            if module_has_submodule(mod, 'serializers'):
                raise

    return user_serializer
