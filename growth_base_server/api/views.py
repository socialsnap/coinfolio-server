import logging
from importlib import import_module

import copy

from django.apps import apps
from django.conf import settings
from django.contrib.admin import site
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseNotFound
from django.utils.module_loading import module_has_submodule
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser, JSONParser

from growth_authentication.models import GrowthUser
from growth_base_server.api.authentication import authenticate_api_token, check_app_version


from growth_base_server.models import ImagePermissions, ApplicationCompany, ApplicationUser
from growth_base_server.api.utils import success_response, require_params, fail_response, login_api_user, \
    find_user_serializer, get_param
from growth_base_server.exceptions import MissingParam


RECORDS_PER_PAGE = 20


@api_view(['GET'])
def api_home(request, format=None):
    return success_response('api home')


@api_view(['POST'])
@check_app_version()
def api_login(request):
    """
    Logs user in and sends back a token to be used on future api calls

    ## Parameters
    **email_address**

    **password**

    ## Returns
        {
            "message": "Optional message to display to user",
            "data": {
                "token": <string>,
                "user_id": <integer>
            }
        }
    """
    try:
        (email_address, password) = require_params(request, ('email', 'password'))
    except MissingParam as e:
        return fail_response('Missing param.', data={'missing_param': e.param})

    user = login_api_user(request, email_address, password)

    if user is None:
        return fail_response('Could not authenticate user')

    serializer = find_user_serializer(user)

    if serializer is not None:
        data = serializer.data
    else:
        data = {
            'token': user.login_token,
            'user_id': user.id,
        }

    message = 'Logged in'
    return success_response(message, data)


@api_view(['POST'])
@parser_classes((JSONParser, MultiPartParser))
@csrf_exempt
def add_image(request, **kwargs):
    """
    Not to be called direct by apps.

    Used as a callback once an image was successfully uploaded to Google cloud, to then store the key with a model.
    """
    pk = get_param(request, 'pk')
    company_id = get_param(request, 'company')
    model_name = get_param(request, 'model')
    app_name = get_param(request, 'app')
    image = get_param(request, 'image')
    image_key = image.content_type_extra['blob-key']
    field = get_param(request, 'field')

    logging.debug('LOGO_ in callback for ' + pk)
    logging.debug('LOGO_ request: ' + str(request))
    logging.debug('LOGO_ image: ' + str(image))
    logging.debug('LOGO_ key: ' + str(image_key))
    logging.debug('LOGO_ model: ' + str(model_name))
    logging.debug('LOGO) user: ' + str(request.user))

    try:
        model = apps.get_app_config(app_name).get_model(model_name=model_name)
    except LookupError:
        return fail_response('Could not find model ' + model_name)

    try:
        item = model.objects.get(pk=pk)
    except model.DoesNotExist:
        return fail_response('Model object does not exist ' + pk)

    setattr(item, field + '_blobstore_key', image_key)
    item.save()

    if company_id:
        try:
            company = ApplicationCompany.objects.get(pk=company_id)
        except ApplicationUser.DoesNotExist:
            company = None

        # add company permission for this image file
        image_permission = ImagePermissions(company=company,
                                            blobstore_key=image_key)
        image_permission.save()

    return success_response('Uploaded logo')


@api_view(['GET'])
@authenticate_api_token()
def serve_image(request, *args, **kwargs):
    """
    Generally not called directly by apps, used internally to send back a file stored in Google cloud

    ## Parameters

    **key** (the blobstore key, likely sent back by a separate API call)

    ## Returns

    HTTP response containing the image, with appropriate content type set
    """
    (blob_key,) = require_params(request, ['key'])

    file_permission = ImagePermissions.objects.filter(blobstore_key=blob_key, company=request.application_company)
    if not file_permission.exists():
        return HttpResponseNotFound('<h1>Page not found</h1>')

    from google.appengine.api import images
    from google.appengine.api.images import Image, CORRECT_ORIENTATION
    from google.appengine.ext import blobstore

    max_size = get_param(request, 'max_size')

    if max_size is not None:
        max_size = int(max_size)
        image_resize = Image(blob_key=blob_key)
        image_resize.set_correct_orientation(CORRECT_ORIENTATION)
        image_resize.resize(width=max_size, height=max_size)

        thumbnail = image_resize.execute_transforms(output_encoding=images.JPEG, parse_source_metadata=True)
        return HttpResponse(thumbnail, content_type="image/jpeg")

    blob_info = blobstore.get(blob_key)
    with blob_info.open() as f:
        return HttpResponse(f.read(), content_type="image/jpeg")


@api_view(['POST'])
@parser_classes((JSONParser, MultiPartParser))
def add_file(request, *args, **kwargs):
    """
    Generally not called directly by apps, used internally to store an uploaded image in Google cloud

    ## Parameters

    **file**

    application_company_id

    ## Returns

        {
            "data":
            {
                "blobstore_key": blobstore_key
            }
        }
    """
    try:
        application_company_id = get_param(request, 'application_company_id')
        file = get_param(request, 'file')
        blobstore_key = str(file.blobstore_info.key())
        created = file.blobstore_info.creation

        if application_company_id:
            image_permission = ImagePermissions(company_id=application_company_id, blobstore_key=blobstore_key)
            image_permission.save()

        return_data = {
            'blobstore_key': blobstore_key,
        }
        return success_response('Saved uploaded file', data=return_data)
    except ObjectDoesNotExist:
        return fail_response("Could not save image")


@api_view(['GET'])
@authenticate_api_token()
def serve_file(request, *args, **kwargs):
    """
    Generally not called directly by apps, used internally to send back a file stored in Google cloud

    ## Parameters

    **key** (the blobstore key, likely sent back by a separate API call)

    ## Returns

    HTTP response containing the file, with appropriate content type set
    """
    (blob_key,) = require_params(request, ['key'])
    application_company_id = get_param(request, 'application_company_id')

    if application_company_id:
        file_permission = ImagePermissions.objects.filter(blobstore_key=blob_key, company=request.application_company)
        if not file_permission.exists():
            return HttpResponseNotFound('<h1>Page not found</h1>')

    from google.appengine.ext import blobstore

    blob_info = blobstore.get(blob_key)
    with blob_info.open() as f:
        return HttpResponse(f.read(), content_type=blob_info.content_type)


class ActivateAccountView(TemplateView):
    template_name = 'user/activate.html'
    success_message = 'Your account has been activated.</h1><p>You can now log into the mobile application.'
    failure_message = 'This activation token does not belong to this account.'

    def get_context_data(self, **kwargs):
        context = super(ActivateAccountView, self).get_context_data(**kwargs)

        token = self.request.GET.get('token')
        (user_id, activation_token) = token.split('--')

        try:
            user = GrowthUser.objects.get(id=user_id)
        except ObjectDoesNotExist:
            context['message'] = self.failure_message
            return context

        if user.check_reset_password_token(activation_token):
            user.is_active = True
            user.save()
            context['message'] = self.success_message
        else:
            context['message'] = self.failure_message

        return context
