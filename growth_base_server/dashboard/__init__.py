from django.conf import settings

from growth_base_server.dashboard.dashboard_menu import DashboardMenu


class DashboardConfig(object):
    menu = None
    dashboard_title = 'GrowthCreate'
    default_dashboard_subtitle = ''
    default_application_name = 'GrowthCreate'
    scripts = ['js/module.js',]
    stylesheets = ['css/module.css',]
    show_list_actions = True
    new_text = 'Create New'
    save_button_text = 'Save'
    cancel_button_text = 'Cancel'
    additional_context = {}

    # set as a dict of {group_name: [menu item list]} if user is in multiple groups, last menu here wins
    user_menus = {}

    def __init__(self):
        self.menu = None

    def set_stylesheets(self, stylesheet_list):
        self.stylesheets = stylesheet_list

    def append_stylesheet(self, stylesheet):
        if stylesheet not in self.stylesheets:
            self.stylesheets.append(stylesheet)

    def set_scripts(self, script_list):
        self.scripts = script_list

    def append_script(self, script):
        if script not in self.scripts:
            self.scripts.append(script)

    def remove_script(self, script):
        try:
            self.scripts.remove(script)
        except ValueError:
            pass

    def add_user_menu(self, user_type, menu):
        self.user_menus[user_type] = menu

    def add_context(self, context):
        self.additional_context.update(context)

    def get_dashboard_title(self):
        if hasattr(self, 'dashboard_title'):
            return self.dashboard_title
        elif hasattr(settings, 'APPLICATION_DISPLAY_NAME'):
            return settings.APPLICATION_DISPLAY_NAME
        else:
            return self.default_application_name

    def get_dashboard_subtitle(self):
        if hasattr(self, 'dashboard_subtitle'):
            return self.dashboard_subtitle
        elif hasattr(settings, 'APPLICATION_DISPLAY_SUBTITLE'):
            return settings.APPLICATION_DISPLAY_SUBTITLE
        else:
            return self.default_dashboard_subtitle

    def get_base_context(self, request):
        context = {
            'dashboard_title': self.get_dashboard_title(),
            'dashboard_subtitle': self.get_dashboard_subtitle(),
            'show_list_actions': self.show_list_actions,
            'new_text': self.new_text,
            'module_scripts': self.scripts,
            'module_stylesheets': self.stylesheets,
            'save_button_text': self.save_button_text,
            'cancel_button_text': self.cancel_button_text,
        }

        if not request.user.is_authenticated:
            return context

        if request.user.is_staff:
            menu = self.user_menus.get('Staff', None)
        elif request.user.groups.count() > 0:
            group = request.user.groups.last()
            menu = self.user_menus.get(group.name, None)
        else:
            menu = self.user_menus.get('Admin', None)

        if menu:
            self.menu = DashboardMenu(*menu)
            menu_items = self.menu.items
        else:
            menu_items = []

        for item in menu_items:
            selected = request.path in item[1]
            item.append(selected)

        context.update({'dashboard_menu_items': menu_items})
        context.update(self.additional_context)

        return context


dashboard_config = DashboardConfig()
