from django import forms
from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView

from growth_authentication.models import GrowthUser

from growth_base_server.dashboard import dashboard_config
from growth_base_server.dashboard.forms import UserForm
from growth_base_server.dashboard.mixins import DashboardMixin
from growth_base_server.emails import send_set_password_email


class DashboardView(DashboardMixin, TemplateView):
    template_name = 'dashboard/pd_dashboard_base.html'
    action = ''


class DashboardFormView(DashboardMixin, FormView):
    template_name = 'dashboard/pd_dashboard_base.html'
    action = ''


class GenericListView(DashboardMixin, ListView):
    template_name = 'dashboard/pd_dashboard_list.html'
    action = 'list'
    list_headings = []
    list_actions = [
        ('update', 'fa-pencil-square-o'),
        ('delete', 'fa-times-circle'),
    ]


class GenericUpdateView(DashboardMixin, UpdateView):
    template_name = 'dashboard/pd_dashboard_edit_form.html'
    action = 'Update'


class GenericCreateView(DashboardMixin, CreateView):
    template_name = 'dashboard/pd_dashboard_object_form.html'
    action = 'Create new'
    hide_create_link = False

    list_headings = []
    list_actions = [
        ('update', 'fa-pencil-square-o'),
        ('delete', 'fa-times-circle'),
    ]


class GenericDeleteView(DashboardMixin, DeleteView):
    template_name = 'dashboard/pd_dashboard_confirm_delete.html'
    action = 'Delete'


class UserScreen(GenericCreateView):
    model = GrowthUser
    form_class = UserForm
    list_fields = ['first_name', 'last_name', 'email']

    def post(self, request, *args, **kwargs):
        response = super(UserScreen, self).post(request, *args, **kwargs)

        # load up the User that should have been saved automatically above
        new_user = self.object

        return response

    def send_set_password(self, user):
        token = user.generate_reset_password_token()
        send_set_password_email(user, token)


class UserUpdateScreen(GenericUpdateView):
    model = GrowthUser
    form_class = UserForm

    def get_form(self, form_class=None):
        form = super(UserUpdateScreen, self).get_form(form_class)
        form.fields['password'].widget = forms.HiddenInput()

        return form


class UserDeleteScreen(GenericDeleteView):
    model = GrowthUser
    fields = ['first_name', 'last_name', 'email_address']


def handler404(request):
    dashboard_context = dashboard_config.get_base_context(request)
    context_instance = RequestContext(request)
    context_instance.update(dashboard_context)

    response = render('404.html', context=context_instance)
    response.status_code = 404

    return response


def handler500(request):
    dashboard_context = dashboard_config.get_base_context(request)
    context_instance = RequestContext(request)
    context_instance.update(dashboard_context)

    response = render('500.html', context=context_instance)
    response.status_code = 500

    return response
