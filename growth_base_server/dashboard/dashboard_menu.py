from django.urls import reverse_lazy


class DashboardMenu(object):
    items = []

    def __init__(self, *args, **kwargs):
        self.items = []
        for arg in args:
            item = [arg.title(), reverse_lazy(arg)]
            self.items.append(item)

    def append_item(self, label, uri=None):
        if not uri:
            uri = reverse_lazy(label)
        item = [label.title(), uri]
        if item in self.items:
            self.items.remove(item)
        self.items.append(item)
