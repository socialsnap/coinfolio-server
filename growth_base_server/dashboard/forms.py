from datetime import timedelta, datetime

from django import forms
from django.forms import ModelForm
from django.utils import formats

from growth_authentication.models import GrowthUser


class LocalizedDateTimeWidget(forms.SplitDateTimeWidget):
    # TODO: set format from settings.DATETIME_FORMAT
    format = '%Y-%m-%d %H:%M'
    offset = None
    any_time = False

    def __init__(self, offset=None, *args, **kwargs):
        self.offset = offset
        self.any_time = False
        super(LocalizedDateTimeWidget, self).__init__()

    def set_offset(self, offset):
        """
        :param offset: timezone offset in minutes
        """
        self.offset = offset

    def set_any_time(self, any_time):
        """
        :param any_time: any_time boolean flag
        """
        self.any_time = any_time

    # for display
    def decompress(self, value):
        if self.offset is None:
            raise ValueError('Cannot localize datetime. LocalizedDateTimeWidget has no offset defined.')
        if value is not None:
            if self.any_time:
                display_offset = 0
            else:
                display_offset = self.offset
            datetime_obj = datetime.strptime(value, self.format) - timedelta(minutes=display_offset, )
            return datetime_obj.strftime('%Y-%m-%d %H:%M').split(' ')
        return super(LocalizedDateTimeWidget, self).decompress(value)


class LocalizedDateTimeField(forms.DateTimeField):
    widget = LocalizedDateTimeWidget
    input_formats = formats.get_format_lazy('DATETIME_INPUT_FORMATS')


class UserForm(ModelForm):
    class Meta:
        model = GrowthUser
        fields = ['first_name', 'last_name', 'email', 'password', ]

    def clean_email(self):
        email_address = self.cleaned_data['email']
        other_accounts = GrowthUser.objects.filter(email=email_address)

        email_is_used = False
        if self.instance.pk:
            other_accounts = other_accounts.exclude(pk=self.instance.pk)

        for other_account in other_accounts:
            if other_account.is_active:
                email_is_used = True

        if email_is_used:
            raise forms.ValidationError("An account already exists for this email address.")

        return email_address
