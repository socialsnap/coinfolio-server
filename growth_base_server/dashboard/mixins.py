import logging
import re

import requests
from django import forms
from django.core.exceptions import FieldDoesNotExist
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.urls import reverse, NoReverseMatch
from django.views.generic.base import ContextMixin
from requests_toolbelt import MultipartEncoder

from growth_base_server.api.utils import extend_request_timeout, reset_request_timeout
from growth_base_server.dashboard import dashboard_config
from growth_base_server.dashboard.utils import get_company


class ReadOnlyFormMixin(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ReadOnlyFormMixin, self).__init__(*args, **kwargs)
        for key in self.fields.keys():
            self.fields[key].disabled = True

    def save(self, *args, **kwargs):
        # do not do anything
        pass


class DashboardMixin(ContextMixin):
    exclude_fields = ['id', 'ordering']
    """
    default page size is 20. Override for different numbers of records per page.
    """
    page_size = 20

    # dashboard view utils
    def disable_form(self, form):
        for field in form:
            field.field.widget.attrs.update({'readonly': 'readonly', 'disabled': ''})
        return form

    def admin_user(self):
        if self.request.user.is_staff:
            return True
        else:
            return False

    def _get_list_fields(self):
        if not hasattr(self, 'list_fields'):
            self.list_fields = self.get_fields_from_meta()
        return self.list_fields

    def get_list_headings(self):
        if not hasattr(self, 'list_headings') or (hasattr(self, 'list_headings') and len(self.list_headings) == 0):
            # set list headings from self.fields set in implementations
            list_headings = []

            for list_field in self._get_list_fields():
                try:
                    list_headings.append(self.model._meta.get_field(list_field).verbose_name)
                except FieldDoesNotExist:
                    list_headings.append(list_field.title())
        else:
            list_headings = self.list_headings
        return list_headings

    def get_fields_from_meta(self):
        list_fields = []
        for field in self.model._meta.fields:
            if field.name not in self.exclude_fields:
                list_fields.append(field.name)
        return list_fields

    def get_context_data(self, **kwargs):
        context = super(DashboardMixin, self).get_context_data(**kwargs)

        context.update(dashboard_config.get_base_context(self.request))
        context.update({'page_title': self.get_page_title()})

        if hasattr(self, 'module_stylesheets'):
            if 'module_stylesheets' in context:
                context['module_stylesheets'].append(self.module_stylesheets)
            else:
                context.update({'module_stylesheets': self.module_stylesheets})

        if 'form' in context:
            if not hasattr(self, 'form_fields'):
                form_fields = []
            else:
                form_fields = self.form_fields

            context['form'] = self.form_defaults(context['form'], form_fields)

        if hasattr(self, 'model'):
            dynamic_context = {
                'object_list': self.get_queryset_paged(),
                'object_title': self.get_model_name().title(),
                'page_title': self.get_page_title(),
                'cancel_uri': self.get_module_list_uri(),
                'form_panel_title': self.action + ' ' + self.get_model_name(),
                'form_only': self.form_only if hasattr(self, 'form_only') else False,
                'show_list_actions': self.show_list_actions if hasattr(self, 'show_list_actions') else True,
                'list_actions': self.list_actions if hasattr(self, 'list_actions') else [],
                'list_headings': self.get_list_headings(),
                'new_text': self.new_text if hasattr(self, 'new_text') else context['new_text'],
                'form_disabled': self.form_disabled if hasattr(self, 'form_disabled') else False,
                'hide_create_link': self.hide_create_link if hasattr(self, 'hide_create_link') else True
            }
            context.update(dynamic_context)

            if hasattr(self, 'object') and hasattr(self, 'sub_models'):
                sub_objects = {}
                for sub_model in self.sub_models:
                    sub_objects[sub_model] = getattr(self.object, sub_model+'s').all()
                context.update({'sub_models': self.sub_models, 'sub_objects': sub_objects})

        return context

    def get_queryset_paged(self):
        if not hasattr(self, 'object_list') or (hasattr(self, 'object_list') and self.object_list is None):
            self.object_list = self.get_queryset()

        if not self.object_list.ordered:
            self.object_list = self.object_list.order_by('pk')
            
        paginator = Paginator(self.object_list, self.page_size)

        page = self.request.GET.get('p')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            objects = paginator.page(1)
        except EmptyPage:
            # If page is out of range deliver last page of results.
            objects = paginator.page(paginator.num_pages)

        return objects

    def get_model_name(self):
        if hasattr(self, 'model'):
            return self.model._meta.verbose_name
        return ''

    def get_model_class_name(self):
        if hasattr(self, 'model'):
            return self.model.__name__
        return ''

    def get_page_title(self):
        return self.get_model_plural_name().title()

    def get_model_plural_name(self):
        if hasattr(self, 'model'):
            return self.model._meta.verbose_name_plural
        return ''

    def post(self, request, *args, **kwargs):
        response = super(DashboardMixin, self).post(request, *args, **kwargs)
        if hasattr(self, 'get_form_class'):
            if not self.get_form(self.get_form_class()).is_valid():
                return response
            if self.get_form(self.get_form_class()).is_multipart():
                self.process_uploads()
        return response

    def form_defaults(self, form, fields=None):
        for field in form:
            if fields and field.name not in fields:
                field.field.widget = forms.HiddenInput()

            if 'class' in field.field.widget.attrs:
                field.field.widget.attrs['class'] = 'form-control ' + field.field.widget.attrs['class']
            else:
                field.field.widget.attrs.update({'class': 'form-control'})

        return form

    def get_module_list_uri(self):
        """
        Returns the uri for the module's list view if:
            - your list view's url pattern is named for the model's plural name OR
            - your list view's url pattern looks like '/dashboard/{model name}/'
        If your list view uri differs from these, overwrite this function in your dashboard view class
        """
        try:
            # first - if a dashboard url is named for the model's plural name
            return reverse(self.model._meta.verbose_name_plural)
        except NoReverseMatch:
            # second - if there is a url with pattern '/dashboard/{object_stub}/'
            class_name = self.get_model_class_name()
            model_stub_words = re.findall('[A-Z][a-z]*', class_name)
            model_stub = '_'.join(model_stub_words).lower()

            return '/dashboard/' + model_stub + '/'

    def process_uploads(self):
        form = self.get_form(self.get_form_class())

        for field in form:
            if field.field.widget.__class__.__name__ in ('ClearableFileInput', 'FileInput'):
                if field.name in self.request.FILES:
                    image_file = self.request.FILES[field.name]

                    model = form._meta.model
                    self.process_uploaded_image(field, form, image_file, model)

    def process_uploaded_image(self, field, form, image_file, model):
        process_image(image_file, model._meta.model_name, model._meta.app_label, form.instance.pk,
                      field.name, 'add-image')

    def get_success_url(self):
        return self.get_module_list_uri()


class ApplicationUserMixin(DashboardMixin):
    exclude_fields = ['id', 'is_deleted', 'application_company', 'last_updated', 'ordering']
    """
    default page size is 20. Override for different numbers of records per page.
    """
    page_size = 20
    """
    by default dashboard pages manage company-specific data. Use admin_page=True for a page that manages admin data
    """
    admin_page = False

    def get(self, request, *args, **kwargs):
        if not self.admin_page:
            return self.fail_render_for_superusers()
        else:
            return super(ApplicationUserMixin, self).get(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(ApplicationUserMixin, self).get_queryset()
        qs = qs.filter(application_company_id=self.request.user.application_user.application_company_id)

        return qs

    def get_context_data(self, **kwargs):
        context = super(ApplicationUserMixin, self).get_context_data(**kwargs)

        context.update({'company': get_company(self.request), })

        if hasattr(self, 'filter_form'):
            context.update({'filter_form': self.get_filter_form()})

        return context

    def get_form(self, form_class=None):
        form = super(ApplicationUserMixin, self).get_form(form_class)
        if self.admin_page:
            return form

        return self.limit_model_field_choices(form)

    def get_filter_form(self):
        filter_form = self.filter_form(self.request.GET)
        self.limit_model_field_choices(filter_form)

        return filter_form

    def user_company(self):
        return self.request.user.application_user.application_company

    def limit_model_field_choices(self, form):
        application_company_id = self.request.user.application_user.application_company_id
        for field in form.fields:
            if form.fields[field].__class__.__name__ == "ModelChoiceField" and field != "application_company":
                model = form.fields[field].queryset.model
                if hasattr(model, 'application_company_id'):
                    # model records belong to an application company, so filter options appropriately
                    form.fields[field].queryset = model.objects.all().filter(
                        application_company_id=application_company_id)
                else:
                    # this model is general-purpose, so don't restrict it
                    pass
        return form

    def fail_render_for_superusers(self):
        self.object = None
        self.queryset = []
        self.template_name = 'dashboard/pd_dashboard_message.html'

        message_details = 'In order to manage ' + str(
            self.get_model_plural_name()) + ', your user must have a defined company.'

        context_data = dashboard_config.get_base_context(self.request)
        context_data['dashboard_message'] = 'This page cannot be rendered for superusers.'
        context_data['dashboard_message_details'] = message_details

        return self.render_to_response(context_data)

    def form_defaults(self, form, fields=None):
        super(ApplicationUserMixin, self).form_defaults(form, fields)
        
        for field in form:
            if field.name in ['is_deleted', 'application_company', 'last_updated', 'ordering']:
                field.field.widget = forms.HiddenInput()

        return form

    def process_uploaded_image(self, field, form, image_file, model):
        process_image(image_file, model._meta.model_name, model._meta.app_label, form.instance.pk,
                      field.name, 'add-image', company_id=self.request.user.application_user.application_company_id)


def process_image(image_file, model_name, app_name, object_pk, field, callback_url, company_id=None, image_type=None):
    logging.debug('LOGO_ adding image for ' + model_name + ' ' + str(object_pk))
    extend_request_timeout()
    if image_type is None:
        image_type = 'image/jpeg'
    image_data = __format_image_for_upload(image_file, image_type)

    from google.appengine.ext import blobstore
    upload_url = blobstore.create_upload_url(reverse(callback_url))
    encoder = MultipartEncoder(fields={
        'pk': str(object_pk),
        'image': (str(model_name) + str(object_pk), image_data, image_type),
        'model': str(model_name),
        'app': str(app_name),
        'field': str(field),
        'company': str(company_id)
    })

    requests.post(upload_url, data=encoder, headers={'Content-Type': encoder.content_type})

    reset_request_timeout()


def __format_image_for_upload(image_file, image_type):
    data = image_file.read()
    upfile = SimpleUploadedFile(image_file.name, data, image_type)
    image_data = upfile.read()
    return image_data
