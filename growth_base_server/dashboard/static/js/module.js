$(document).ready(function(){
    var message_check_interval = 3000; //3seconds

    // prepare list screen by hiding the form until called
    if(!$("#form-panel").hasClass('show-form')) {
        $("#form-panel").hide();
    }
    else
    {
        $( "#create-link-panel").hide();
    }

    // show form
    $("#create-link").click(function(){
        if( $( "#form-panel").is(":hidden" ) ) {
            $( "#form-panel").slideDown( "fast");
            $( "#create-link-panel").slideUp("fast");
            $('form:first *:input[type!=hidden]:first').focus();
        }
    });

    // hide form
    $(".cancel-button").click(function(){
        if( $( "#form-panel").is(":visible" ) ) {
            $( "#form-panel").slideUp( "fast");
            $( "#create-link-panel").slideDown("fast");
        }
    });
});
