from django import forms
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.utils import six

from growth_authentication.models import GrowthUser


def get_company(request):
    if settings.RESTRICT_API_BY_COMPANY is False:
        return None

    if hasattr(request.user, 'growth_user'):
        return request.user.application_user.application_company

    return None


def user_group_required(function=None, group=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    if isinstance(group, six.string_types):
        group = [group, ]
    actual_decorator = user_passes_test(
        lambda u: u.groups.filter(name__in=group).count() > 0,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def create_dashboard_user(email_address, password, first_name, last_name, is_active=True):
    user = GrowthUser(
        first_name=first_name,
        last_name=last_name,
        is_active=is_active,
        email=email_address,
        password=password,
        is_api_user=False,
        is_dashboard_user=True,
    )
    user.save()

    return user
