from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import send_mail


def get_postmark_setting(setting_name):
    if not hasattr(settings, setting_name):
        raise ImproperlyConfigured('Tried to use "' + str(setting_name) + '" setting, but there was no value set.')
    return getattr(settings, setting_name)


def send_reset_password_email(user, reset_token):
    host_name = get_postmark_setting('HOSTNAME')
    reset_url = 'https://' + str(host_name) + '/reset-pw/?token=' + str(user.pk) + '--' + str(reset_token)
    app_name = settings.APPLICATION_DISPLAY_NAME

    email_body = """
        %s has received a password reset request for '%s'.

        To reset your password, click the following link:
        %s

        If you did not request a new password, you can safely disregard this email.

    """ % (app_name, user.email_address, reset_url)
    postmark_sender = get_postmark_setting('POSTMARK_SENDER')
    send_mail('Password reset request', email_body, postmark_sender,
              [user.email], fail_silently=False)

    return True


def send_set_password_email(user, reset_token):
    hostname = get_postmark_setting('HOSTNAME')
    android_download_uri = get_postmark_setting('ANDROID_DOWNLOAD_URI')
    ios_download_uri = get_postmark_setting('IOS_DOWNLOAD_URI')
    postmark_sender = get_postmark_setting('POSTMARK_SENDER')
    reset_url = 'https://' + str(hostname) + '/reset-pw/?token=' + str(user.pk) + '--' + str(reset_token)
    app_name = settings.APPLICATION_DISPLAY_NAME

    android_download_text = "Download for Android: %s \n\n" % (android_download_uri,) if android_download_uri else ""

    ios_download_text = "Download for iOS: %s \n\n" % (ios_download_uri,) if ios_download_uri else ""

    email_body = """
        You have a new account for %s.

        Username: %s

        To set your password, click the following link:
        %s

        Once you have set your password, you will be able to log in to the app.

        %s
        %s

    """ % (app_name, user.username, reset_url, android_download_text, ios_download_text)

    send_mail('Set new password', email_body, postmark_sender,
              [user.email], fail_silently=True)

    return True


def send_new_admin_email(user, reset_token):
    hostname = get_postmark_setting('HOSTNAME')
    postmark_sender = get_postmark_setting('POSTMARK_SENDER')
    reset_url = 'https://' + str(hostname) + '/reset-pw/?token='+str(user.pk) + '--' + str(reset_token)
    app_name = settings.APPLICATION_DISPLAY_NAME

    email_body = """
        Welcome to %s! Your account has already been created, so you simply need to set your password to get started.

        Username: %s
        Click here to set your password.
        %s

        Once you have set your password, you will be able to log in.

        The team at %s
        %s

    """ % (app_name, user.email_address, reset_url, app_name, hostname)

    send_mail('Set new password', email_body, postmark_sender,
              [user.email], fail_silently=True)

    return True


def send_activate_account_email(user, reset_token):
    host_name = get_postmark_setting('HOSTNAME')
    url = 'https://' + str(host_name) + '/activate/?token=' + str(user.pk) + '--' + str(reset_token)
    app_name = settings.APPLICATION_DISPLAY_NAME

    email_body = """
        Hello, %s,

        You have created a new account with %s.

        To activate your account, click the following link:
        %s

        If you did not create a new account, you can safely disregard this email.

    """ % (user.get_full_name(), app_name, url)
    postmark_sender = get_postmark_setting('POSTMARK_SENDER')
    send_mail('Account activation', email_body, postmark_sender,
              [user.email], fail_silently=False)

    return True
