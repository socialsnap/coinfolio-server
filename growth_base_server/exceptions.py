from django.core.exceptions import ObjectDoesNotExist


class MissingParam(ImportError):
    trip_id = None

    def __init__(self, value, param, expected_params):
        self.value = value
        self.param = param
        self.expected_params = expected_params

    def __str__(self):
        return str(self.value)


class InvalidRelationship(ObjectDoesNotExist):
    invalid_id = None

    def __init__(self, value, invalid_id):
        self.value = value
        self.invalid_id = invalid_id

    def __str__(self):
        return str(self.value)
