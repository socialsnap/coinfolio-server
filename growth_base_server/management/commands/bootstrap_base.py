# coding: utf-8
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.add_superuser()

    def add_superuser(self):
        email = 'projects@growthcreate.com'
        password = 'KaliLinux007'

        self.stdout.write(self.style.SUCCESS('Creating superuser'))

        user_model = get_user_model()
        if not user_model.objects.filter(is_superuser=True).exists():
            u = user_model(username=email)
            u.set_password(password)
            u.is_superuser = True
            u.is_staff = True
            u.save()
