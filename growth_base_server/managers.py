from django.db import models


class ArchivableQuerySet(models.QuerySet):
    def updated_since(self, last_sync):
        return self.filter(last_updated__gte=last_sync).order_by('last_updated')

    def all_with_deleted(self):
        qs = super(ArchivableQuerySet, self).all()

        return qs


class ArchivableModelManager(models.Manager.from_queryset(ArchivableQuerySet)):
    def get_queryset(self):
        qs = super(ArchivableModelManager, self).get_queryset()

        qs = qs.filter(is_deleted=False)

        return qs

    def all_with_deleted(self):
        qs = super(ArchivableModelManager, self).get_queryset()

        if hasattr(self, 'core_filters'):  # it's a RelatedManager
            qs = qs.filter(**self.core_filters)

        return qs

    def get(self, *args, **kwargs):
        if 'pk' in kwargs or 'id' in kwargs:
            return self.all_with_deleted().get(*args, **kwargs)
        else:
            return self.get_queryset().get(*args, **kwargs)

    def filter(self, *args, **kwargs):
        if 'pk' in kwargs or 'id' in kwargs:
            qs = self.all_with_deleted().filter(*args, **kwargs)
        else:
            qs = self.get_queryset().filter(*args, **kwargs)

        return qs


class ApplicationUserManager(ArchivableModelManager):
    def get_queryset(self):
        qs = super(ApplicationUserManager, self).get_queryset()

        return qs.select_related('user', 'application_company')
