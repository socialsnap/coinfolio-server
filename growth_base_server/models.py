import socket

from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models

from growth_authentication.models import GrowthUser
from growth_base_server.managers import ApplicationUserManager, ArchivableModelManager

API_MODE = 'api'
DASHBOARD_MODE = 'dashboard'


class ArchivableModel(models.Model):
    is_deleted = models.BooleanField(default=False)
    last_updated = models.DateTimeField(auto_now=True)

    objects = ArchivableModelManager()

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()


class ApplicationCompany(models.Model):
    name = models.CharField(max_length=200)
    growth_notification_identifier = models.CharField(max_length=200, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    logo_blobstore_key = models.CharField(max_length=200, null=True, blank=True)
    turn_off_growth_footer = models.BooleanField(default=False)

    LOGO_URL_PATTERN = 'http://%s/images/%s/200'
    DEFAULT_LOGO = ""

    class Meta:
        verbose_name_plural = "application companies"

    def __str__(self):
        return self.name

    @property
    def logo_uri(self):
        if self.logo_blobstore_key is not None and self.logo_blobstore_key != '':
            try:
                HOSTNAME = socket.gethostname()
            except:
                HOSTNAME = 'localhost'

            return self.LOGO_URL_PATTERN % (HOSTNAME, self.logo_blobstore_key)
        else:
            return None

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        result = super(ApplicationCompany, self).save(force_insert, force_update, using, update_fields)
        if not self.logo_blobstore_key:
            return result
        # set image permissions
        has_image_permission = ImagePermissions.objects.filter(company=self, blobstore_key=self.logo_blobstore_key)
        if not len(has_image_permission):
            image_permission = ImagePermissions(company=self, blobstore_key=self.logo_blobstore_key)
            image_permission.save()
        return result


class ApplicationUser(ArchivableModel):
    user = models.OneToOneField(GrowthUser, related_name="application_user", on_delete=models.CASCADE)
    application_company = models.ForeignKey(ApplicationCompany, related_name='application_users', null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'application company user'
        ordering = ['user__last_name', 'user__first_name']
    objects = ApplicationUserManager()

    # override output for dashboard display w/o affecting api values
    def get_display_attr(self, item, growth_user=None):
        return getattr(self, item)

    @property
    def valid_company(self):
        if settings.RESTRICT_API_BY_COMPANY is False:
            return True
        if self.application_company.is_active:
            return True
        return False

    @property
    def growth_notification_identifier(self):
        if hasattr(self, 'application_company') and self.application_company:
            return self.application_company.growth_notification_identifier
        else:
            return ''

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

        self.user.is_active = False
        self.user.login_token = None
        self.user.save()

    def add_to_group(self, group_name):
        group = Group.objects.get(name=group_name)
        self.user.groups.add(group)


class ImagePermissions(models.Model):
    company = models.ForeignKey(ApplicationCompany, on_delete=models.CASCADE)
    is_public = models.BooleanField()
    blobstore_key = models.CharField(max_length=255)
