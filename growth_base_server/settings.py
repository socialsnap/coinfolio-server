import socket

from django.conf import settings

if not hasattr(settings, 'GROWTH_LOGIN_TEMPLATE'):
    setattr(settings, 'GROWTH_LOGIN_TEMPLATE', 'dashboard/pd_dashboard_login.html')

if not hasattr(settings, 'LOGIN_URL'):
    setattr(settings, 'LOGIN_URL', '/dashboard/login/')

try:
    HOSTNAME = socket.gethostname()
except:
    HOSTNAME = 'localhost'

if not hasattr(settings, 'API_IMG_URI'):
    setattr(settings, 'API_IMG_URI', 'http://' + HOSTNAME + '/api/image/?key=%s' )

if not hasattr(settings, 'DASHBOARD_IMG_URI'):
    setattr(settings, 'DASHBOARD_IMG_URI', 'http://' + HOSTNAME + '/images/%s')

if not hasattr(settings, 'API_FILE_URI'):
    setattr(settings, 'API_FILE_URI', 'http://' + HOSTNAME + '/api/file/?key=%s' )

if not hasattr(settings, 'DASHBOARD_FILE_URI'):
    setattr(settings, 'DASHBOARD_FILE_URI', 'http://' + HOSTNAME + '/files/%s')

if not hasattr(settings, 'RESTRICT_API_BY_COMPANY'):
    setattr(settings, 'RESTRICT_API_BY_COMPANY', True)

if not hasattr(settings, 'IMAGE_URI_PATTERN'):
    setattr(settings, 'IMAGE_URI_PATTERN', 'http://%s/images/%s/')

if not hasattr(settings, 'THUMBNAIL_URI_PATTERN'):
    setattr(settings, 'THUMBNAIL_URI_PATTERN', 'http://%s/images/%s/200c')
