from django.conf import settings

CHUNK_SIZE = 1024


def upload_file(file):
    from google.cloud import storage
    from google.cloud.exceptions import NotFound, GoogleCloudError

    import urllib3
    urllib3.disable_warnings()

    storage_client = storage.Client()

    bucket_name = settings.USER_UPLOAD_BUCKET_NAME
    try:
        bucket = storage_client.get_bucket(bucket_name)
    except NotFound:
        import logging
        logging.error("Could not find storage bucket")
        logging.error(bucket_name)
        return None, None

    upload_name = generate_file_name(file)
    blob = bucket.blob(upload_name)

    try:
        blob.upload_from_file(file, content_type=file.content_type)
    except GoogleCloudError as gce:
        import logging
        logging.error("Error occurred during upload")
        logging.error(gce)
        return None, None

    return blob.public_url, upload_name


def generate_file_name(file):
    from django.utils.timezone import now

    current_datetime = now()
    prefix = current_datetime.strftime('%Y%m%d%H%M%S')
    upload_name = '_'.join([prefix, file.name])

    return upload_name


def delete_file(blob_name, bucket_name=None):
    from google.cloud import storage
    from google.cloud.exceptions import NotFound

    import urllib3

    urllib3.disable_warnings()

    storage_client = storage.Client()

    if bucket_name is None:
        from django.conf import settings
        bucket_name = settings.USER_UPLOAD_BUCKET_NAME

    try:
        bucket = storage_client.get_bucket(bucket_name)
    except NotFound:
        import logging
        logging.error("Could not find storage bucket")
        logging.error(bucket_name)
        return None

    try:
        bucket.delete_blob(blob_name)
    except NotFound:
        import logging
        logging.error("Blob no longer exists for deletion")
        logging.error(blob_name)
        return None


def delete_files_by_date(bucket_name, from_date):
    from google.cloud import storage
    from google.cloud.exceptions import NotFound

    import urllib3
    import pytz

    urllib3.disable_warnings()

    storage_client = storage.Client()
    from_date = pytz.utc.localize(from_date)

    try:
        bucket = storage_client.get_bucket(bucket_name)
    except NotFound:
        import logging
        logging.error("Could not find storage bucket")
        logging.error(bucket_name)
        return None

    blobs = bucket.list_blobs()
    blobs_to_delete = []
    for blob in blobs:
        if blob.time_created.replace(tzinfo=pytz.utc) < from_date:
            blobs_to_delete.append(blob)

    try:
        bucket.delete_blobs(blobs_to_delete)
    except NotFound:
        import logging
        logging.error("Blob no longer exists for deletion")
        return None
