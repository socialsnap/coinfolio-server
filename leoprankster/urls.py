"""leoprankster URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path

from accounts.urls import accounts_api_patterns
# from exchanges.urls import exchanges_api_patterns
from portfolios.urls import portfolios_api_pattern

api_patterns = [
    path('accounts', include((accounts_api_patterns, 'api-accounts'))),
    # path('exchanges', include((exchanges_api_patterns, 'api-exchanges'))),
    path('portfolios', include((portfolios_api_pattern, 'api-portfolios'))),
]

urlpatterns = [
    path(r'admin/', admin.site.urls),
    re_path(r'^api/v(?P<version>\d+)/', include(api_patterns)),
]

if 'growth_base_server.api' in settings.INSTALLED_APPS:
    from growth_base_server.api.urls import api_patterns as base_patterns

    urlpatterns.append(re_path(r'^api/', include(base_patterns)))

# we generally don't want DEBUG to be on when deployed to beta, as it slows the site down
# so we test the PRODUCTION flag to ensure docs show on beta but not on live
# if not settings.PRODUCTION:
#     urlpatterns.append(path(r'^docs/', include_docs_urls(title='leoprankster Server API', public=False)))

if settings.DEBUG:
    import debug_toolbar

    debug_patterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

    urlpatterns = debug_patterns + urlpatterns
