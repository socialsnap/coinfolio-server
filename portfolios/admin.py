from django.contrib import admin

from portfolios.models import Token, Wallet, Asset

admin.site.register(Asset)
admin.site.register(Token)
admin.site.register(Wallet)
