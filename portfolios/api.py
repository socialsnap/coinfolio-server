from rest_framework import status
from rest_framework.decorators import api_view

from growth_base_server.api.utils import fail_response, success_response, require_params
from growth_base_server.exceptions import MissingParam
from leoprankster.settings import DEBUG
from portfolios.models import Wallet, Token, Asset, ExchangeRate
from portfolios.utils.bsc_utils import bsc_token_info, bsc_asset_add
from portfolios.utils.eth_utils import eth_token_info
from portfolios.utils.json import assets_json, token_time_series_json
from portfolios.utils.sms import notify_admins


@api_view(['POST', 'GET'])
def token_history(request, **kwargs):
    try:
        required_params = ('wallet_address', 'contract')
        wallet_address, contract = require_params(request, required_params)
    except MissingParam as mp:
        return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)

    contract = contract.lower()
    wallet_address = wallet_address.lower()

    try:
        asset = Asset.objects.get(contract__iexact=contract)
    except Asset.DoesNotExist:
        return fail_response('Asset does not exist.', status_code=status.HTTP_404_NOT_FOUND)

    wallet, _ = Wallet.objects.get_or_create(address=wallet_address)

    try:
        token_data = bsc_token_info(contract, wallet_address, pair_contract=asset.pair_contract) \
            if asset.blockchain_network == Asset.BLOCKCHAIN_NETWORK_BSC else eth_token_info(contract, wallet_address)
    except Exception as error:
        return fail_response(f'{str(error)}', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    token, token_created = Token.objects.get_or_create(asset=asset, wallet=wallet)

    if token_created:
        token.time_series_data = [token_data]
    else:
        token.time_series_data.append(token_data)

    token.save()
    data = token_time_series_json(asset, token.time_series_data)

    return success_response('Token Time Series Data.', data=data)


@api_view(['POST', 'GET'])
def assets_info(request, **kwargs):
    assets = Asset.objects.all()
    data = assets_json(assets)

    return success_response('All Assets.', data=data)


@api_view(['POST', 'GET'])
def add_asset(request, **kwargs):
    try:
        required_params = ('contract', 'blockchain_network')
        contract, blockchain_network = require_params(request, required_params)
    except MissingParam as mp:
        return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)

    if blockchain_network not in Asset.blockchain_networks():
        return fail_response('Invalid blockchain network.', status_code=status.HTTP_400_BAD_REQUEST)

    try:
        asset_info = bsc_asset_add(contract)
    except Exception as e:
        return fail_response('Invalid Contract', status_code=status.HTTP_400_BAD_REQUEST)

    name = asset_info['name']
    symbol = asset_info['symbol']
    pair_contract = asset_info['pair_contract']

    asset, created = Asset.objects.get_or_create(name=name, symbol=symbol,
                                                 contract=contract,
                                                 pair_contract=pair_contract,
                                                 blockchain_network=blockchain_network)

    if not created:
        return fail_response('Token already available.', status_code=status.HTTP_400_BAD_REQUEST)

    if not DEBUG:
        try:
            notify_admins(message=f'''
                              New Asset request
                              Name: {name}
                              Symbol: {symbol}
                              Contract:{contract}
                              Network: {blockchain_network}
                              ''')
        except Exception as e:
            print(str(e))

    asset_info['contract'] = asset.contract
    asset_info['verified'] = asset.verified
    asset_info['is_active'] = asset.is_active
    return success_response('Asset created.', data=asset_info)


@api_view(['POST', 'GET'])
def activate_asset(request, **kwargs):
    try:
        required_params = ('contract',)
        contract, = require_params(request, required_params)
    except MissingParam as mp:
        return fail_response('Missing param', data={'missing_param': mp.param}, status_code=status.HTTP_400_BAD_REQUEST)

    contract = contract.lower()

    try:
        asset = Asset.objects.get(contract=contract)
    except Asset.DoesNotExist:
        return fail_response('Asset does not exist.', status_code=status.HTTP_404_NOT_FOUND)

    asset.is_active = True
    asset.save()

    assets = Asset.objects.all()
    data = assets_json(assets)

    return success_response('Asset is now active.', data=data)


@api_view(['POST', 'GET'])
def exchange_rates(request, **kwargs):
    exchange_rate = ExchangeRate.objects.first()

    if exchange_rate is None:
        return fail_response('Exchange Rate does not exist.', status_code=status.HTTP_404_NOT_FOUND)

    return success_response('Exchange Rates.', data=exchange_rate.info)
