from rest_framework import status
from rest_framework.decorators import api_view

from growth_base_server.api.utils import success_response, fail_response
from portfolios.models import ExchangeRate
from portfolios.utils.utils import conversion_rate_info


@api_view(['POST', 'GET'])
def update_conversion_rate(request, **kwargs):
    try:
        conversion_rates_info = conversion_rate_info()
    except Exception as error:
        return fail_response(f'{str(error)}', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    exchange_rate = ExchangeRate.objects.first()
    if exchange_rate is None:
        exchange_rate = ExchangeRate.objects.create()

    exchange_rate.info = conversion_rates_info
    exchange_rate.save()

    return success_response('Currency Conversion Rate updated.', data=exchange_rate.info)
