import json

from django.core.management import BaseCommand

from growth_authentication.models import GrowthUser
from portfolios.models import Wallet, Token, Asset


class Command(BaseCommand):
    help = f'python manage.py populate_db_user_portfolio_data'

    def handle(self, *args, **options):
        file_name = 'user_portfolio_data.json'
        print(f'Populating DB with data from {file_name}')

        with open(f'portfolios/fixtures/{file_name}', 'r') as f:
            data = json.loads(f.read())

            for item in data:
                wallet_address = item['wallet_address'].lower()
                portfolio_data = item['portfolio_data']

                asset = Asset.objects.get(contract__iexact='0x9c9d4302a1a550b446401e56000f76bc761c3a33')

                user = GrowthUser.objects.create(username=wallet_address, email=wallet_address, is_api_user=True)

                wallet = Wallet.objects.create(address=wallet_address, user=user)

                Token.objects.create(asset=asset, wallet=wallet, time_series_data=portfolio_data)
                print(f'Added Wallet:{wallet_address} data')

        print('Portfolio DB Updated')
