# coding: utf-8
from django.core.management import BaseCommand
from portfolios.models import Asset, Token, Wallet, ExchangeRate


class Command(BaseCommand):
    help = f"python manage.py wallet_insights"

    def handle(self, *args, **options):
        self.user_assets()

    @staticmethod
    def user_assets():
        wallets_count = Wallet.objects.count()
        wallets = Wallet.objects.all()[wallets_count - 100: wallets_count]
        for wallet in wallets:
            assets = wallet.assets.all()
            print(f"{'*' * 60}")
            print(f"Wallet: f{wallet} Asset{assets}")
            print(f"==============================================================================")
            print(f"{'*' * 60}")
