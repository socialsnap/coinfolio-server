# Generated by Django 3.1 on 2021-07-23 07:06

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('portfolios', '0008_asset_verified'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='asset',
            name='updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
