from django.db import models

from growth_authentication.models import GrowthUser


class Asset(models.Model):
    BLOCKCHAIN_NETWORK_BSC = 'binance'
    BLOCKCHAIN_NETWORK_ETH = 'ethereum'

    BLOCKCHAIN_NETWORK_CHOICES = [
        (BLOCKCHAIN_NETWORK_BSC, 'Binance'),
        (BLOCKCHAIN_NETWORK_ETH, 'Ethereum'),
    ]

    class Meta:
        db_table = 'coinfolio_asset'
        verbose_name_plural = 'assets'

    name = models.CharField(max_length=128)
    symbol = models.CharField(max_length=128)

    contract = models.CharField(max_length=255, unique=True)
    pair_contract = models.CharField(max_length=255, unique=True, null=True, blank=True)
    blockchain_network = models.CharField(max_length=50, choices=BLOCKCHAIN_NETWORK_CHOICES, null=False, blank=False)

    is_active = models.BooleanField(null=False, default=False)
    verified = models.BooleanField(null=False, default=False)

    created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    updated = models.DateTimeField(null=False, blank=True, auto_now=True)

    def __str__(self):
        return f'{self.name.title()}'

    @classmethod
    def blockchain_networks(cls):
        return [choice[0] for choice in cls.BLOCKCHAIN_NETWORK_CHOICES]


class Wallet(models.Model):
    class Meta:
        db_table = 'coinfolio_wallet'
        verbose_name_plural = 'wallets'

    users = models.ManyToManyField(GrowthUser, blank=True)
    assets = models.ManyToManyField(Asset, through='Token', blank=True)

    address = models.CharField(max_length=255, null=False, blank=False)

    created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    updated = models.DateTimeField(null=False, blank=True, auto_now=True)

    def __str__(self):
        return f'{self.address}'


class Token(models.Model):
    class Meta:
        db_table = 'coinfolio_token'
        verbose_name_plural = 'tokens'

    asset = models.ForeignKey(Asset, null=False, blank=False, on_delete=models.CASCADE)
    wallet = models.ForeignKey(Wallet, null=False, blank=False, on_delete=models.CASCADE)

    time_series_data = models.JSONField(null=True)

    created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    updated = models.DateTimeField(null=False, blank=True, auto_now=True)

    def __str__(self):
        return f'{self.asset.name.title()}'


class ExchangeRate(models.Model):
    class Meta:
        db_table = 'coinfolio_exchange_rate'
        verbose_name_plural = 'exchange_rates'

    info = models.JSONField(null=True, blank=True)
    created = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    updated = models.DateTimeField(null=False, blank=True, auto_now=True)
