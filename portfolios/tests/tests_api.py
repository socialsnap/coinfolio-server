from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status

from portfolios.models import Asset, Token, Wallet, ExchangeRate


class TestUserTokens(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-portfolios:api-asset:api-token:token-history', kwargs={'version': 1})
        self.asset = mommy.make('Asset', name='test-asset', symbol='TESTASSET', contract='0x-test-contract',
                                blockchain_network=Asset.BLOCKCHAIN_NETWORK_BSC)

        self.json = {
            'wallet_address': '0x000000000000000000000000000000',
            'contract': self.asset.contract
        }

    def test_success(self):
        with patch('portfolios.api.bsc_token_info') as mock_bsc_token_info:
            mock_bsc_token_info.return_value = {
                "price_usd": 0.000008504,
                "amount_usd": 1306.4802,
                "amount_tokens": 258985716896032463,
                "date": 1619862660.266559
            }

            response = self.client.post(self.URL_ENDPOINT, self.json)
            assert mock_bsc_token_info.called

            self.assertContains(response, 'Token Time Series Data.', status_code=status.HTTP_200_OK)
            self.assertEquals(Token.objects.count(), 1)
            self.assertEquals(Wallet.objects.count(), 1)

    def test_returns_correct_data(self):
        test_wallet_address = self.json['wallet_address']
        wallet = mommy.make('Wallet', address=test_wallet_address)
        mommy.make('Token', asset=self.asset, wallet=wallet, time_series_data=[{
            "price_usd": 1,
            "amount_usd": 1,
            "amount_tokens": 1,
            "date": 1619862445.266559
        }])

        with patch('portfolios.api.bsc_token_info') as mock_bsc_token_info:
            mock_bsc_token_info.return_value = {
                "price_usd": 2,
                "amount_usd": 2,
                "amount_tokens": 1,
                "date": 1619862660.266559
            }

            response = self.client.post(self.URL_ENDPOINT, self.json)
            assert mock_bsc_token_info.called

            self.assertContains(response, 'Token Time Series Data.', status_code=status.HTTP_200_OK)
            self.assertEquals(response.json()['data']['name'], self.asset.name)
            self.assertEquals(response.json()['data']['symbol'], self.asset.symbol)
            self.assertEquals(response.json()['data']['contract'], self.asset.contract)
            self.assertEquals(len(response.json()['data']['token_history']), 2)

    def test_missing_param(self):
        del self.json['wallet_address']
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_returns_404_when_asset_invalid(self):
        self.json['contract'] = 'invalid-contract'
        response = self.client.post(self.URL_ENDPOINT, self.json)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_returns_500(self):
        with patch('portfolios.api.bsc_token_info') as mock_bsc_token_info:
            mock_bsc_token_info.side_effect = Exception('Pancake Swap Error: error msg')

            response = self.client.post(self.URL_ENDPOINT, self.json)
            assert mock_bsc_token_info.called

            self.assertContains(response, 'Pancake Swap Error: error msg',
                                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
            self.assertEquals(Wallet.objects.count(), 1)
            self.assertEquals(Token.objects.count(), 0)


class TestAssets(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-portfolios:api-asset:all-assets', kwargs={'version': 1})
        self.asset = mommy.make('Asset')

    def test_success(self):
        response = self.client.get(self.URL_ENDPOINT)
        self.assertContains(response, 'All Assets.', status_code=status.HTTP_200_OK)
        self.assertEquals(response.json()['data'][0]['name'], self.asset.name)
        self.assertEquals(response.json()['data'][0]['symbol'], self.asset.symbol)
        self.assertEquals(response.json()['data'][0]['contract'], self.asset.contract)


class TestUpdateExchangeRate(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-portfolios:api-exchange-rate:update', kwargs={'version': 1})
        self.mock_exchange_rate_info = {
            "timestamp": 1621822444,
            "source": "USD",
            "quotes": {
                "USDARS": 94.532425
            }
        }

    def test_success(self):
        mommy.make(ExchangeRate)
        with patch('portfolios.cron.conversion_rate_info') as mock_conversion_rate_info:
            mock_conversion_rate_info.return_value = self.mock_exchange_rate_info
            response = self.client.post(self.URL_ENDPOINT)
            assert mock_conversion_rate_info.called

            self.assertContains(response, 'Currency Conversion Rate updated.', status_code=status.HTTP_200_OK)
            self.assertEquals(response.json()['data'].keys(), self.mock_exchange_rate_info.keys())

    def test_creates_new_exchange_rate(self):
        with patch('portfolios.cron.conversion_rate_info') as mock_conversion_rate_info:
            self.assertEquals(ExchangeRate.objects.count(), 0)
            mock_conversion_rate_info.return_value = self.mock_exchange_rate_info

            response = self.client.post(self.URL_ENDPOINT)
            assert mock_conversion_rate_info.called

            self.assertContains(response, 'Currency Conversion Rate updated.', status_code=status.HTTP_200_OK)
            self.assertEquals(ExchangeRate.objects.count(), 1)


class TestListExchangeRate(TestCase):
    def setUp(self):
        self.URL_ENDPOINT = reverse('api-portfolios:api-exchange-rate:all', kwargs={'version': 1})
        self.exchange_rate_info = {
            "timestamp": 1621822444,
            "source": "USD",
            "quotes": {
                "USDARS": 94.532425
            }
        }

    def test_success(self):
        mommy.make(ExchangeRate, info=self.exchange_rate_info)
        response = self.client.get(self.URL_ENDPOINT)

        self.assertContains(response, 'Exchange Rates.', status_code=status.HTTP_200_OK)
        self.assertEquals(response.json()['data'].keys(), self.exchange_rate_info.keys())

    def test_exchange_range_does_not_exist(self):
        response = self.client.get(self.URL_ENDPOINT)
        self.assertContains(response, 'Exchange Rate does not exist.', status_code=status.HTTP_404_NOT_FOUND)
