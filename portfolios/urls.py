from django.urls import path, include

from portfolios.api import assets_info, token_history, exchange_rates, add_asset, activate_asset
from portfolios.cron import update_conversion_rate

token_api_patterns = ([
    path('/history', token_history, name='token-history'),
], 'api-token')

asset_api_patterns = ([
    path('', assets_info, name='all-assets'),
    path('/token', include(token_api_patterns)),
    path('/new', add_asset, name='create'),
    path('/activate', activate_asset, name='activate'),
], 'api-asset')

exchange_rates_api_patterns = ([
    path('', exchange_rates, name='all'),
    path('/update', update_conversion_rate, name='update'),
], 'api-exchange-rate')

portfolios_api_pattern = [
    path('/assets', include(asset_api_patterns)),
    path('/exchange_rates', include(exchange_rates_api_patterns)),
]
