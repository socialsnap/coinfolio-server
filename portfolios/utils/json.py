def asset_json(asset):
    return {
        'name': asset.name,
        'symbol': asset.symbol,
        'contract': asset.contract,
        'verified': asset.verified,
        'is_active': asset.is_active,
    }


def assets_json(assets):
    return [asset_json(asset) for asset in assets]


def token_time_series_json(asset, time_series_data):
    return {
        **asset_json(asset),
        'token_history': time_series_data
    }
