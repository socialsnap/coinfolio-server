from datetime import datetime, timezone

import requests
from web3 import Web3

from leoprankster.settings import CURRENCY_LAYER_API_KEY


def utc_timestamp_now():
    dt = datetime.now(timezone.utc)
    utc_time = dt.replace(tzinfo=timezone.utc)
    return utc_time.timestamp()


def conversion_rate_info():
    url = f'http://api.currencylayer.com/live?access_key={CURRENCY_LAYER_API_KEY}&format=2'

    try:
        response = requests.request("GET", url)
    except Exception as error:
        raise error

    rates_info = response.json()
    return {
        'timestamp': rates_info['timestamp'],
        'conversion_rates': rates_info['quotes'],
        'source': rates_info['source'],
    }


def token_balance(wallet_address, token_decimals, token_contract):
    wallet_address = Web3.toChecksumAddress(wallet_address)
    account_balance = token_contract.functions.balanceOf(wallet_address).call()
    return account_balance / 10 ** token_decimals
